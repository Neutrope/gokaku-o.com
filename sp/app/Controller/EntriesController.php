<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Entries Controller
 *
 * @property Entry $Entry
 */
class EntriesController extends AppController {
	
	//どのアクションが呼ばれてもはじめに実行される関数
	public function beforeFilter()
	{
		parent::beforeFilter();
	
		$this->Auth->allow('admin_kanri', 'admin_index', 'admin_view', 'admin_add', 'admin_edit', 'admin_delete');
	}
	

/**
 * index method
 *
 * @return void

	public function index() {
		$this->Entry->recursive = 0;
		$this->set('entries', $this->paginate());
	} */

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void

	public function view($id = null) {
		if (!$this->Entry->exists($id)) {
			throw new NotFoundException(__('Invalid entry'));
		}
		$options = array('conditions' => array('Entry.' . $this->Entry->primaryKey => $id));
		$this->set('entry', $this->Entry->find('first', $options));
	} */

/**
 * add method
 *
 * @return void
 */
	public function add($offer_id = null) {
		
		// レイアウトは専用
		$this->layout = 'default_entry';
		
		$this->set('entry_offer_id', $offer_id);
		
		if ($this->request->is('post')) {
			$this->Entry->create();
			
			//mb_convert_variables('UTF-8', 'EUC-JP', $this->request->data['Entry']);
			mb_convert_variables('UTF-8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS,SJIS-win,eucJP-win,ISO-2022-JP,ISO-2022-JP-MS,JIS-ms', $this->request->data['Entry']);
			
			// コントローラ(Controller)からデータのバリデーションを実行する
			$this->Entry->set( $this->request->data );
			
			if ($this->Entry->validates()) {
				// バリデーションが成功した場合のロジックをここに書く
				
				// 保存へ
				if ($this->Entry->save($this->request->data)) {
				
					//$this->Session->setFlash(__('The entry has been saved'));
					//$this->redirect(array('action' => 'index'));
					$this->redirect(array('action' => 'complete', $this->Entry->getInsertID()));
				
				} else {
					//$this->Session->setFlash(__('The entry could not be saved. Please, try again.'));
					//$this->Session->setFlash(__('エントリーに失敗しました。時間を置いて、もう一度エントリーしてください。'));
					
					$error_fatal = true;
					$this->set('error_fatal', $error_fatal);
											
					$offer = $this->Entry->Offer->find('first', array('conditions' => array('Offer.id' => $offer_id)));
					$this->set('offer', $offer);
					$this->set('entry_data', $this->request->data['Entry']); // はじめの入力値をフォームに渡す
				}
				
			} else {
				// バリデーションが失敗した場合のロジックをここに書く
				
				$errors = $this->Entry->invalidFields(); // validationErrors 配列を含むデータを取得する
				$this->set('error_info', $errors);
									
				$offer = $this->Entry->Offer->find('first', array('conditions' => array('Offer.id' => $offer_id)));
				$this->set('offer', $offer);
				$this->set('entry_data', $this->request->data['Entry']); // はじめの入力値をフォームに渡す
			}
			
			
		}else{
			
			$offer = $this->Entry->Offer->find('first', array('conditions' => array('Offer.id' => $offer_id)));
			$this->set('offer', $offer);
		}
	}

	/**
	 * complete method
	 *
	 * @return void
	 */
	public function complete($entry_id = null) {
		// レイアウトは専用
		$this->layout = 'default_entry';
		
		if (!$this->Entry->exists($entry_id)) {
			throw new NotFoundException(__('Invalid entry'));
		}
		$options = array('conditions' => array('Entry.' . $this->Entry->primaryKey => $entry_id));
		$entry_data = $this->Entry->find('first', $options);
		$offer_data = $this->Entry->Offer->find('first', array('conditions' => array('Offer.id' => $entry_data['Offer']['id'])));
		
		$entry_time = new DateTime($entry['Entry']['created']);
		$entry_time->modify("+4 day");
		$hentou_kigen = $entry_time->format("Y-m-d");
		
		// メール内容
        $mailbody = array(
            'hentou_kigen' => $hentou_kigen,
        	'content' => "",
        	'entry' => $entry_data,
        	'offer' => $offer_data,
        );
                
        // メール送信実行
        $email = new CakeEmail('gokakuo');     // ←Config/email.php で編集した配列名を指定
        $sent = $email
            ->template('entry_complete')         // ←テンプレ名
            ->viewVars($mailbody)           // ←メール内容配列をテンプレに渡す
            ->subject('エントリー希望のメール')
            ->send();
        
        /*
        if ( $sent ) {
             echo 'メール送信成功！' ;
        } else {
             echo 'メール送信失敗' ;
        }
		*/
	}
	
	
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void

	public function edit($id = null) {
		if (!$this->Entry->exists($id)) {
			throw new NotFoundException(__('Invalid entry'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Entry->save($this->request->data)) {
				$this->Session->setFlash(__('The entry has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The entry could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Entry.' . $this->Entry->primaryKey => $id));
			$this->request->data = $this->Entry->find('first', $options);
		}
		$offers = $this->Entry->Offer->find('list');
		$this->set(compact('offers'));
	} */

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void

	public function delete($id = null) {
		$this->Entry->id = $id;
		if (!$this->Entry->exists()) {
			throw new NotFoundException(__('Invalid entry'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Entry->delete()) {
			$this->Session->setFlash(__('Entry deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Entry was not deleted'));
		$this->redirect(array('action' => 'index'));
	} */

	
	
	public function admin_kanri(){
	
		// レイアウトは専用
		$this->layout = 'default_login';
	
		if($this->request->is('post')) {
			if($this->Auth->login()){
				return $this->redirect($this->Auth->redirectUrl());
				// 2.3より前なら `return $this->redirect($this->Auth->redirect());`
			}else{
				$this->Session->setFlash('正しいIDとパスワードを入力してください');
			}
		}else{
			$this->Auth->logout();
		}
	}
	
	
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		
		$uid = $this->Auth->user('id');
		if($uid != 1) $this->redirect('kanri');
		
		$this->Entry->recursive = 0;
		$this->set('entries', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		
		$uid = $this->Auth->user('id');
		if($uid != 1) $this->redirect('kanri');
		
		if (!$this->Entry->exists($id)) {
			throw new NotFoundException(__('Invalid entry'));
		}
		$options = array('conditions' => array('Entry.' . $this->Entry->primaryKey => $id));
		$this->set('entry', $this->Entry->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		
		$uid = $this->Auth->user('id');
		if($uid != 1) $this->redirect('kanri');
		
		if ($this->request->is('post')) {
			$this->Entry->create();
			
			//mb_convert_variables('UTF-8', 'EUC-JP', $this->request->data['Entry']);
			mb_convert_variables('UTF-8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS,SJIS-win,eucJP-win,ISO-2022-JP,ISO-2022-JP-MS,JIS-ms', $this->request->data['Entry']);
				
			if ($this->Entry->save($this->request->data)) {
				$this->Session->setFlash(__('The entry has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The entry could not be saved. Please, try again.'));
			}
		}
		$offers = $this->Entry->Offer->find('list');
		$this->set(compact('offers'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		
		$uid = $this->Auth->user('id');
		if($uid != 1) $this->redirect('kanri');
		
		if (!$this->Entry->exists($id)) {
			throw new NotFoundException(__('Invalid entry'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			//mb_convert_variables('UTF-8', 'EUC-JP', $this->request->data['Entry']);
			mb_convert_variables('UTF-8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS,SJIS-win,eucJP-win,ISO-2022-JP,ISO-2022-JP-MS,JIS-ms', $this->request->data['Entry']);
			
			if ($this->Entry->save($this->request->data)) {
				$this->Session->setFlash(__('The entry has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The entry could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Entry.' . $this->Entry->primaryKey => $id));
			$this->request->data = $this->Entry->find('first', $options);
		}
		$offers = $this->Entry->Offer->find('list');
		$this->set(compact('offers'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		
		$uid = $this->Auth->user('id');
		if($uid != 1) $this->redirect('kanri');
		
		$this->Entry->id = $id;
		if (!$this->Entry->exists()) {
			throw new NotFoundException(__('Invalid entry'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Entry->delete()) {
			$this->Session->setFlash(__('Entry deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Entry was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
