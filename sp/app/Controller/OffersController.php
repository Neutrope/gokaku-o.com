<?php
App::uses('AppController', 'Controller');
/**
 * Offers Controller
 *
 * @property Offer $Offer
 */
class OffersController extends AppController {
	
	//どのアクションが呼ばれてもはじめに実行される関数
	public function beforeFilter()
	{
		parent::beforeFilter();
		
		$this->Auth->allow('admin_kanri', 'admin_index', 'admin_view', 'admin_add', 'admin_edit', 'admin_delete');
	}
			
	
/**
 * index method
 *
 * @return void
 */
	public function index() {
		
		// レイアウトは専用
		$this->layout = 'default_offer';
		
		$this->Offer->recursive = 0;
		$this->set('offers', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 
	public function view($id = null) {
		if (!$this->Offer->exists($id)) {
			throw new NotFoundException(__('Invalid offer'));
		}
		$options = array('conditions' => array('Offer.' . $this->Offer->primaryKey => $id));
		$this->set('offer', $this->Offer->find('first', $options));
	}*/

/**
 * add method
 *
 * @return void
 
	public function add() {
		if ($this->request->is('post')) {
			$this->Offer->create();
			if ($this->Offer->save($this->request->data)) {
				$this->Session->setFlash(__('The offer has been saved')); // エラーがおこる
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The offer could not be saved. Please, try again.'));
			}
		}
	}*/

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 
	public function edit($id = null) {
		if (!$this->Offer->exists($id)) {
			throw new NotFoundException(__('Invalid offer'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Offer->save($this->request->data)) {
				$this->Session->setFlash(__('The offer has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The offer could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Offer.' . $this->Offer->primaryKey => $id));
			$this->request->data = $this->Offer->find('first', $options);
		}
	}*/

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 
	public function delete($id = null) {
		$this->Offer->id = $id;
		if (!$this->Offer->exists()) {
			throw new NotFoundException(__('Invalid offer'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Offer->delete()) {
			$this->Session->setFlash(__('Offer deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Offer was not deleted'));
		$this->redirect(array('action' => 'index'));
	}*/

	
	
	public function admin_kanri(){
	
		// レイアウトは専用
		$this->layout = 'default_login';
	
		if($this->request->is('post')) {
			if($this->Auth->login()){
				return $this->redirect($this->Auth->redirectUrl());
				// 2.3より前なら `return $this->redirect($this->Auth->redirect());`
			}else{
				$this->Session->setFlash('正しいIDとパスワードを入力してください');
			}
		}else{
			$this->Auth->logout();
		}
	}
	
	
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		
		$uid = $this->Auth->user('id');
		if($uid != 1) $this->redirect('kanri');
		
		$this->Offer->recursive = 0;
		$this->set('offers', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		
		$uid = $this->Auth->user('id');
		if($uid != 1) $this->redirect('kanri');
		
		if (!$this->Offer->exists($id)) {
			throw new NotFoundException(__('Invalid offer'));
		}
		$options = array('conditions' => array('Offer.' . $this->Offer->primaryKey => $id));
		$this->set('offer', $this->Offer->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		
		$uid = $this->Auth->user('id');
		if($uid != 1) $this->redirect('kanri');
		
		if ($this->request->is('post')) {
			$this->Offer->create();
			
			// checkbox 複数選択の結果の配列を文字列として連結
			if($this->request->data['Offer']['day_week']){
				$this->request->data['Offer']['day_week'] = implode("", $this->request->data['Offer']['day_week']);
			}
			
			//mb_convert_variables('UTF-8', 'EUC-JP', $this->request->data['Offer']);
			mb_convert_variables('UTF-8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS,SJIS-win,eucJP-win,ISO-2022-JP,ISO-2022-JP-MS,JIS-ms', $this->request->data['Offer']);
				
			if ($this->Offer->save($this->request->data)) {
				//$this->Session->setFlash(__('The offer has been saved'));
				$this->Session->setFlash(__('オファーを追加しました'));
				$this->redirect(array('action' => 'index'));
			} else {
				//$this->Session->setFlash(__('The offer could not be saved. Please, try again.'));
				$this->Session->setFlash(__('オファーの追加に失敗しました。しばらく待ってもう一度登録してください。'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		
		$uid = $this->Auth->user('id');
		if($uid != 1) $this->redirect('kanri');
		
		if (!$this->Offer->exists($id)) {
			throw new NotFoundException(__('Invalid offer'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			//mb_convert_variables('UTF-8', 'EUC-JP', $this->request->data['Offer']);
			mb_convert_variables('UTF-8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS,SJIS-win,eucJP-win,ISO-2022-JP,ISO-2022-JP-MS,JIS-ms', $this->request->data['Offer']);
			if ($this->Offer->save($this->request->data)) {
				//$this->Session->setFlash(__('The offer has been saved'));
				$this->Session->setFlash(__('オファーを更新しました'));
				$this->redirect(array('action' => 'index'));
			} else {
				//$this->Session->setFlash(__('The offer could not be saved. Please, try again.'));
				$this->Session->setFlash(__('オファーの更新に失敗しました。しばらく待ってもう一度更新してください。'));
			}
		} else {
			$options = array('conditions' => array('Offer.' . $this->Offer->primaryKey => $id));
			$this->request->data = $this->Offer->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		
		$uid = $this->Auth->user('id');
		if($uid != 1) $this->redirect('kanri');
		
		$this->Offer->id = $id;
		if (!$this->Offer->exists()) {
			throw new NotFoundException(__('Invalid offer'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Offer->delete()) {
			//$this->Session->setFlash(__('Offer deleted'));
			$this->Session->setFlash(__('オファーを削除しました'));
			$this->redirect(array('action' => 'index'));
		}
		//$this->Session->setFlash(__('Offer was not deleted'));
		$this->Session->setFlash(__('オファーを削除できませんでした'));
		$this->redirect(array('action' => 'index'));
	}
}
