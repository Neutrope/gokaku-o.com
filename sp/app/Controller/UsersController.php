<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController {
	
	//読み込むコンポーネントの指定
	public $components = array('Session',
			'Auth' => array(
			'loginAction' => array('controller' => 'users','action' => 'login'), //ログインを行なうaction
                       'loginRedirect' => array('controller' => 'offers', 'action' => 'index'), //ログイン後のページ
                       'logoutRedirect' => array('controller' => 'users', 'action' => 'login'), //ログアウト後のページ
			'authError'=>'ログインして下さい。',
						));
		
	//どのアクションが呼ばれてもはじめに実行される関数
	public function beforeFilter()
	{
		parent::beforeFilter();
		
		$this->Auth->allow('logout', 'login', 'index');
	}
	
	public function entries(){
		$this->redirect('/entries/');
	}
	
	public function login(){
		
		// レイアウトは専用
		$this->layout = 'default_login';
		
		if($this->request->is('post')) {
			if($this->Auth->login()){
				return $this->redirect($this->Auth->redirectUrl());
				// 2.3より前なら `return $this->redirect($this->Auth->redirect());`
			}else{
				$this->Session->setFlash('正しいパスワードを入力してください');
			}
		}else{
			if($this->Auth->login()){
				return $this->redirect($this->Auth->redirectUrl());
			}
		}
	}
		
	
	public function logout(){
		$this->Auth->logout();
		$this->redirect('login');
	}

	public function index(){
		$this->set('user', $this->Auth->user());
		$this->redirect('/offers');
	}

}
