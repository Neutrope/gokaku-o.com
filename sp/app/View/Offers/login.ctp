			<h1>ログイン</h1>
<form action="/sp/offers/login" id="UserLoginForm" method="post" accept-charset="utf-8">
<div style="display:none;"><input name="_method" value="POST" type="hidden"></div>

<div class="input text"><label class="ui-input-text" for="UserUsername">Username</label><input class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset" name="data[User][username]" maxlength="255" value="" id="UserUsername" type="text"></div>

<div class="input password"><label class="ui-input-text" for="UserPassword">Password</label><input class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset" name="data[User][password]" value="" id="UserPassword" type="password"></div>

<div class="submit">
<input aria-disabled="false" class="ui-btn-hidden" value="Submit" type="submit">

</div></form>