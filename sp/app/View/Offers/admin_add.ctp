<div class="offers form">
<?php echo $this->Form->create('Offer'); ?>
	<fieldset>
		<legend><?php echo __('オファー追加'); ?></legend>
	<?php
		//echo $this->Form->input('station');
		echo $this->Form->input('station', array(
				'label' => '最寄駅'
		));
		
		//echo $this->Form->input('grade');
		echo $this->Form->label('grade', '学年');
		echo $this->Form->select('grade',
				array('1' => '小1', '2' => '小2', '3' => '小3', '4' => '小4', '5' => '小5', '6' => '小6',
						'7' => '中1', '8' => '中2', '9' => '中3',
						'10' => '高1', '11' => '高2', '12' => '高3', '13' => 'その他'),
				array('empty'=>false)
		);
		echo "<br><br>";
		
		//echo $this->Form->input('gender');
		echo $this->Form->label('gender', '性別');
		echo $this->Form->select('gender',
				array('1' => '男',
						'2' => '女'),
				array('empty'=>false)
		);
		echo "<br><br>";
		
		//echo $this->Form->input('frequency');
		echo $this->Form->label('frequency', '頻度');
		echo $this->Form->select('frequency',
				array('1' => '週1', '2' => '週2', '3' => '週3', '4' => '週4以上'),
				array('empty'=>false)
		);
		echo "<br><br>";
		
		//echo $this->Form->input('day_week');
		//echo $this->Form->checkbox('day_week', array('checked' => true));
		echo $this->Form->input('day_week', array(
				'label' => '希望曜日（複数選択可）',
				'type' => 'select',
				'multiple'=> 'checkbox',
				'options' => array('月' => '月', '火' => '火', '水' => '水', '木' => '木', '金' => '金', '土' => '土', '日' => '日'),
		));
		
		//echo $this->Form->input('detail');
		echo $this->Form->input('detail', array(
				'label' => 'ニーズ'
		));
		//echo $this->Form->input('kanri_no');
		echo $this->Form->input('kanri_no', array(
				'label' => '管理ナンバー'
		));
	?>
	</fieldset>
<?php echo $this->Form->end(__('登録')); ?>
</div>
<div class="actions">
	<h3><?php echo __('操作'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('オファー一覧'), array('action' => 'index')); ?></li>
		<!-- <li><?php echo $this->Html->link(__('List Entries'), array('controller' => 'entries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Entry'), array('controller' => 'entries', 'action' => 'add')); ?> </li> -->
	</ul>
</div>
