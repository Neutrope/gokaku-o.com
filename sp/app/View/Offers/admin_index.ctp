<div class="offers index">
	<h2><?php echo __('オファー一覧'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('station'); ?></th>
			<th><?php echo $this->Paginator->sort('grade'); ?></th>
			<th><?php echo $this->Paginator->sort('gender'); ?></th>
			<th><?php echo $this->Paginator->sort('frequency'); ?></th>
			<th><?php echo $this->Paginator->sort('day_week'); ?></th>
			<th><?php echo $this->Paginator->sort('detail'); ?></th>
			<th><?php echo $this->Paginator->sort('kanri_no'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
		<tr>
			<th><?php echo "ID"; ?></th>
			<th><?php echo "最寄駅"; ?></th>
			<th><?php echo "学年"; ?></th>
			<th><?php echo "性別"; ?></th>
			<th><?php echo "頻度"; ?></th>
			<th><?php echo "希望曜日"; ?></th>
			<th><?php echo "ニーズ"; ?></th>
			<th><?php echo "管理ナンバー"; ?></th>
			<th><?php echo "登録日時"; ?></th>
			<th><?php echo "更新日時"; ?></th>
			<th><?php echo "操作"; ?></th>
	</tr>
		<?php foreach ($offers as $offer): ?>
	<tr>
		<td><?php echo h($offer['Offer']['id']); ?>&nbsp;</td>
		<td><?php echo h($offer['Offer']['station']); ?>&nbsp;</td>
		<td><?php 
		//echo h($offer['Offer']['grade']);
		switch ($offer['Offer']['grade']) {
				case 1:
					echo "小1";
					break;
				case 2:
					echo "小2";
					break;
				case 3:
					echo "小3";
					break;
				case 4:
					echo "小4";
					break;
				case 5:
					echo "小5";
					break;
				case 6:
					echo "小6";
					break;
				case 7:
					echo "中1";
					break;
				case 8:
					echo "中2";
					break;
				case 9:
					echo "中3";
					break;
				case 10:
					echo "高1";
					break;
				case 11:
					echo "高2";
					break;
				case 12:
					echo "高3";
					break;
				case 13:
					echo "その他";
					break;
		}
		?>&nbsp;</td>
		
		<td><?php 
		//echo h($offer['Offer']['gender']); 
		switch ($offer['Offer']['gender']) {
			case 1:
				echo "男";
				break;
			case 2:
				echo "女";
				break;
		}
		?>&nbsp;</td>
		
		<td><?php 
		//echo h($offer['Offer']['frequency']);
		switch ($offer['Offer']['frequency']) {
			case 1:
				echo "週1";
				break;
			case 2:
				echo "週2";
				break;
			case 3:
				echo "週3";
				break;
			case 4:
				echo "週4以上";
				break;
		}
		 ?>&nbsp;</td>
		 
		<td><?php echo h($offer['Offer']['day_week']); ?>&nbsp;</td>
		<td><?php echo h($offer['Offer']['detail']); ?>&nbsp;</td>
		<td><?php echo h($offer['Offer']['kanri_no']); ?>&nbsp;</td>
		<td><?php echo h($offer['Offer']['created']); ?>&nbsp;</td>
		<td><?php echo h($offer['Offer']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<!--<?php echo $this->Html->link(__('View'), array('action' => 'view', $offer['Offer']['id'])); ?>-->
			<?php echo $this->Html->link(__('編集'), array('action' => 'edit', $offer['Offer']['id'])); ?>
			<?php echo $this->Form->postLink(__('削除'), array('action' => 'delete', $offer['Offer']['id']), null, __('本当に削除しますか？（※削除したデータを元に戻すことはできません） #ID %s', $offer['Offer']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('操作'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('オファー追加'), array('action' => 'add')); ?></li>
		<!-- li><?php echo $this->Html->link(__('List Entries'), array('controller' => 'entries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Entry'), array('controller' => 'entries', 'action' => 'add')); ?> </li> -->
	</ul>
</div>
