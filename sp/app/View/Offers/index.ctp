<!--▼▼トップページ埋め込み時はここから以下をコピーして任意の場所に貼り付けてください（html部は編集可）▼▼-->
<div id="news_wrap">
<ul id="news_list">


</ul>
</div>
<!--▲▲トップページ埋め込み時　コピーここまで▲▲-->

<table width="100%" cellspacing="1">
  <tr>
    <td colspan="3" style="padding:5px; border:1px #999 solid; background-color:#FFF">最寄駅</td>
    <td width="20%" rowspan="3" align="center" style="padding:5px; border:1px #999 solid; background-color:#F0F0F0">エントリー希望を送る</td>
  </tr>
  <tr>
    <td width="20%" style="padding: 5px; border:1px #999 solid; text-align: center; background-color:#FFF">学年</td>
    <td width="12%" style="padding: 5px; border:1px #999 solid; text-align: center; background-color:#FFF">性別</td>
    <td style="padding: 5px; border:1px #999 solid; text-align: center; background-color:#FFF">希望曜日</td>
    </tr>
  <tr>
    <td colspan="3" style="padding:5px; border:1px #999 solid; background-color:#FFF">ニーズ</td>
    </tr>
</table>

<?php
 $bg_color = '#F0F0FF';
?>
<?php foreach ($offers as $offer): ?>
<?php
if($offer['Offer']['gender'] == 2){
$bg_color = '#FFF0F0';
}else{
$bg_color = '#F0F0FF';
}
?>
<table width="100%" cellspacing="1">
  <tr>
    <td colspan="3" style="padding:5px; border:1px #999 solid; background-color:<?php echo $bg_color; ?>"><?php echo h($offer['Offer']['station']); ?></td>
    <td width="20%" rowspan="3" align="center" style="padding:5px; border:1px #999 solid; background-color:#F0F0F0"><a href="entries/add/<?php echo h($offer['Offer']['id']); ?>/">エントリー希望を送る</a></td>
  </tr>
  <tr>
    <td width="20%" style="padding: 5px; border:1px #999 solid; text-align: center; background-color:<?php echo $bg_color; ?>"><?php 
		switch ($offer['Offer']['grade']) {
				case 1:
					echo "小1";
					break;
				case 2:
					echo "小2";
					break;
				case 3:
					echo "小3";
					break;
				case 4:
					echo "小4";
					break;
				case 5:
					echo "小5";
					break;
				case 6:
					echo "小6";
					break;
				case 7:
					echo "中1";
					break;
				case 8:
					echo "中2";
					break;
				case 9:
					echo "中3";
					break;
				case 10:
					echo "高1";
					break;
				case 11:
					echo "高2";
					break;
				case 12:
					echo "高3";
					break;
				case 13:
					echo "その他";
					break;
		}
		?></td>
    <td width="12%" style="padding: 5px; border:1px #999 solid; text-align: center; background-color:<?php echo $bg_color; ?>"><?php 
		switch ($offer['Offer']['gender']) {
			case 1:
				echo "男";
				break;
			case 2:
				echo "女";
				break;
		}
		?></td>
    <td style="padding: 5px; border:1px #999 solid; text-align: center; background-color:<?php echo $bg_color; ?>"><?php 
		switch ($offer['Offer']['frequency']) {
			case 1:
				echo "週1";
				break;
			case 2:
				echo "週2";
				break;
			case 3:
				echo "週3";
				break;
			case 4:
				echo "週4以上";
				break;
		}
		 ?>：<?php echo h($offer['Offer']['day_week']); ?></td>
    </tr>
  <tr>
    <td colspan="3" style="padding:5px; border:1px #999 solid; background-color:<?php echo $bg_color; ?>"><?php echo h($offer['Offer']['detail']); ?></td>
    </tr>
</table>
<?php endforeach; ?>

</div>