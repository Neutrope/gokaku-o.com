<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, user-scalable=no">
<title>家庭教師＆個別指導の合格王</title>
<link href="css/import.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.1.0/jquery.mobile-1.1.0.min.css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/config.js"></script>
<script type="text/javascript" src="http://code.jquery.com/mobile/1.1.0/jquery.mobile-1.1.0.min.js"></script>
<!--▼▼CSS。埋め込み時　要コピペ（head部分）CSSはお好みで自由に編集してください▼▼-->
<style type="text/css">
/* CSSはお好みで */
#news_list li{
	color:#666;
	font-size:14px;
	margin:0;
	padding:10px 5px;
	margin-bottom:3px;
	border-bottom:1px dotted #ccc;
	line-height:150%;
	list-style-type:none;
}
#news_list ul{
	margin:0 0 15px;
	padding:0;
}
#news_list a{color:#36F;text-decoration:underline;}
#news_list a:hover{color:#039;text-decoration:none;}
#news_list li a{display:block;}
</style>

<!--▲▲CSS。埋め込み時　要コピペ（head部分）▲▲-->
</head>	
<body id="b">
<!--<div class="maxw"><img src="img/banner02_o.png" alt="合格体験記"></div>-->
<div data-role="header" data-position="inline" data-theme="a"><a rel="external" data-ajax="false" href="javascript:history.back();" data-theme="a" data-corners="false">back</a><h1>講師の紹介</h1></div>
<div class="maxw"><img src="img/main01_teacher.png" alt="続々と集まる喜びの声"></div>
<div data-role="content">


<?php echo $this->fetch('content'); ?>

		<a rel="external" data-ajax="false" href="tel:0120041590">
		<div class="maxw"><img src="img/banner-footer02.png" alt="0120-041-590"></div>
		</a>
<ul data-role="listview" data-theme="d">
	<li><a rel="external" data-ajax="false" href="area.html"><h3>対応エリア</h3></a></li>
	<!--<li><a rel="external" data-ajax="false" href="service/service_form.php"><h3>無料お見積相談サービス</h3></a></li>-->
	<li><a rel="external" data-ajax="false" href="contact/index.php"><h3>お問い合わせ・資料請求</h3></a></li>
	<li><a rel="external" data-ajax="false" href="profile.html"><h3>会社案内</h3></a></li>
	<li><a rel="external" data-ajax="false" href="policy.html"><h3>プライバシーポリシー</h3></a></li>
</ul>
	<!--</div>--><!-- /content -->
<div style="clear: both;"></div>
<div data-role="footer" data-theme="c"><p class="center">&copy;Copyright Gokakuoh Inc. All rights reserved.</p></div>
</div><!-- /page -->
</body>
</html>

