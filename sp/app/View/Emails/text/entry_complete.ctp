※オファーリストからエントリー希望のメールが届きました。

<?php echo $entry['Entry']['created']; ?> 
---------------------------------------------
▼対象のオファー
オファーID : <?php echo $entry['Offer']['id']; ?> 
管理ナンバー : <?php echo $offer['Offer']['kanri_no']; ?> 
最寄駅 : <?php echo $offer['Offer']['station']; ?> 
学年 : <?php 
		switch ($offer['Offer']['grade']) {
				case 1:
					echo "小1";
					break;
				case 2:
					echo "小2";
					break;
				case 3:
					echo "小3";
					break;
				case 4:
					echo "小4";
					break;
				case 5:
					echo "小5";
					break;
				case 6:
					echo "小6";
					break;
				case 7:
					echo "中1";
					break;
				case 8:
					echo "中2";
					break;
				case 9:
					echo "中3";
					break;
				case 10:
					echo "高1";
					break;
				case 11:
					echo "高2";
					break;
				case 12:
					echo "高3";
					break;
				case 13:
					echo "その他";
					break;
		}
		?> 
性別 : <?php 
		switch ($offer['Offer']['gender']) {
			case 1:
				echo "男";
				break;
			case 2:
				echo "女";
				break;
		}
		?> 
頻度 : <?php 
		switch ($offer['Offer']['frequency']) {
			case 1:
				echo "週1";
				break;
			case 2:
				echo "週2";
				break;
			case 3:
				echo "週3";
				break;
			case 4:
				echo "週4以上";
				break;
		}
		?> 
希望曜日 : <?php echo $offer['Offer']['day_week']; ?> 
詳細 : <?php echo $offer['Offer']['detail']; ?> 


▼エントリー情報
エントリーID : <?php echo $entry['Entry']['id']; ?> 
氏名 : <?php echo $entry['Entry']['full_name']; ?> 
性別 : <?php switch ($entry['Entry']['gender']) {
			case 1:
				echo "男";
				break;
			case 2:
				echo "女";
				break;
		} ?> 
お住まいの最寄り駅 : <?php echo $entry['Entry']['school']; ?> 
電話番号 : <?php echo $entry['Entry']['tel']; ?> 
メールアドレス : <?php echo $entry['Entry']['email']; ?> 
その他 : <?php echo $entry['Entry']['detail']; ?> 
---------------------------------------------

<?php echo $hentou_kigen; ?> までに返答してください。
<?php echo $content; ?>