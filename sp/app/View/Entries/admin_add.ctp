<div class="entries form">
<?php echo $this->Form->create('Entry'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Entry'); ?></legend>
	<?php
		echo $this->Form->input('offer_id');
		//echo $this->Form->input('full_name');
		echo $this->Form->input('full_name', array(
				'label' => '氏名'
		));
		//echo $this->Form->input('gender');
		echo $this->Form->label('gender', '性別');
		echo $this->Form->select('gender',
				array('1' => '男',
						'2' => '女'),
				array('empty'=>false)
		);
		//echo $this->Form->input('school');
		echo $this->Form->input('school', array(
				'label' => 'お住まいの最寄り駅'
		));
		//echo $this->Form->input('school_status');
		echo $this->Form->select('school_status',
				array('1' => '在学中',
						'2' => '卒業'),
				array('empty'=>false)
		);
		//echo $this->Form->input('tel');
		echo $this->Form->input('tel', array(
				'label' => '電話番号'
		));
		//echo $this->Form->input('email');
		echo $this->Form->input('email', array(
				'label' => 'メールアドレス'
		));
		//echo $this->Form->input('detail');
		echo $this->Form->input('detail', array(
				'label' => 'その他'
		));		
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Entries'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Offers'), array('controller' => 'offers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Offer'), array('controller' => 'offers', 'action' => 'add')); ?> </li>
	</ul>
</div>
