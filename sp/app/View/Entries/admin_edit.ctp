<div class="entries form">
<?php echo $this->Form->create('Entry'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Entry'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('offer_id');
		echo $this->Form->input('full_name');
		echo $this->Form->input('gender');
		echo $this->Form->input('school');
		echo $this->Form->input('school_status');
		echo $this->Form->input('tel');
		echo $this->Form->input('email');
		echo $this->Form->input('detail');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Entry.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Entry.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Entries'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Offers'), array('controller' => 'offers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Offer'), array('controller' => 'offers', 'action' => 'add')); ?> </li>
	</ul>
</div>
