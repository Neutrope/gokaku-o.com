<?php
 $bg_color = '#F0F0FF';
if($offer['Offer']['gender'] == 2){
$bg_color = '#FFF0F0';
}else{
$bg_color = '#F0F0FF';
}
?>
<div class="offers index">
<table width="100%" cellspacing="1">
  <tr>
    <td colspan="3" style="padding:5px; border:1px #999 solid; background-color:<?php echo $bg_color; ?>"><?php echo $offer['Offer']['station']; ?></td>
    <td width="20%" rowspan="3" align="center" style="padding:5px; border:1px #999 solid; background-color:#F0F0F0">オファーNo. <?php echo $offer['Offer']['id']; ?> ／管理No. <?php echo $offer['Offer']['kanri_no']; ?></td>
  </tr>
  <tr>
    <td width="20%" style="padding: 5px; border:1px #999 solid; text-align: center; background-color:<?php echo $bg_color; ?>"><?php
		switch ($offer['Offer']['grade']) {
			case 1:
				echo "小1";
				break;
			case 2:
				echo "小2";
				break;
			case 3:
				echo "小3";
				break;
			case 4:
				echo "小4";
				break;
			case 5:
				echo "小5";
				break;
			case 6:
				echo "小6";
				break;
			case 7:
				echo "中1";
				break;
			case 8:
				echo "中2";
				break;
			case 9:
				echo "中3";
				break;
			case 10:
				echo "高1";
				break;
			case 11:
				echo "高2";
				break;
			case 12:
				echo "高3";
				break;
			case 13:
				echo "その他";
				break;
		}
		 ?></td>
    <td width="12%" style="padding: 5px; border:1px #999 solid; text-align: center; background-color:<?php echo $bg_color; ?>"><?php
		switch ($offer['Offer']['gender']) {
			case 1:
				echo "男";
				break;
			case 2:
				echo "女";
				break;
		}
		?></td>
    <td style="padding: 5px; border:1px #999 solid; text-align: center; background-color:<?php echo $bg_color; ?>"><?php
		switch ($offer['Offer']['frequency']) {
			case 1:
				echo "週1";
				break;
			case 2:
				echo "週2";
				break;
			case 3:
				echo "週3";
				break;
			case 4:
				echo "週4以上";
				break;
		}
		?>：<?php echo $offer['Offer']['day_week']; ?></td>
    </tr>
  <tr>
    <td colspan="3" style="padding:5px; border:1px #999 solid; background-color:<?php echo $bg_color; ?>"><?php echo $offer['Offer']['detail']; ?></td>
    </tr>
</table>

</div>

<div class="entries form">

<?php echo $this->Form->create('Entry'); ?>
	<fieldset>
		<legend><?php echo __('上記のオファーにエントリーします'); ?></legend><br>
		<?php echo $this->Form->hidden('offer_id', array('value'=>$entry_offer_id)); ?>

<?php if($error_fatal) echo '<div><h3>エントリーに失敗しました。時間を置いて、もう一度エントリーしてください。</h3></div>' ?>

<?php if($error_info) echo '<div><h3>入力にエラーがあります。</h3>' ?>
<?php if($error_info['full_name']) echo '<p class="error_messe">「氏名」は必須項目です</p>' ?>
<?php if($error_info['tel']) echo '<p class="error_messe">「電話番号」は必須項目です</p>' ?>
<?php if($error_info['email']) echo '<p class="error_messe">「メールアドレス」は必須項目です</p>' ?>
<?php if($error_info) echo '</div>' ?>

<div class="input text required"><label class="ui-input-text" for="EntryFullName">氏名</label>
<input class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset" name="data[Entry][full_name]" maxlength="50" id="EntryFullName" required="required" type="text" value="<?php if($entry_data['full_name']) echo $entry_data['full_name'] ?>">
</div>

<label class="ui-select" for="EntryGender">性別</label>
<div class="ui-select"><div class="ui-btn ui-shadow ui-btn-corner-all ui-fullsize ui-btn-block ui-btn-icon-right ui-btn-up-c" data-mini="false" data-inline="false" data-theme="c" data-iconpos="right" data-icon="arrow-d" data-wrapperels="span" data-iconshadow="true" data-shadow="true" data-corners="true">
<select name="data[Entry][gender]" id="EntryGender">
<option value="1" <?php if($entry_data['gender'] && $entry_data['gender']=='1') echo "selected" ?>>男</option>
<option value="2" <?php if($entry_data['gender'] && $entry_data['gender']=='2') echo "selected" ?>>女</option>
</select></div></div>

<div class="input text">
<label class="ui-input-text" for="EntrySchool">お住まいの最寄り駅</label>
<input class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset" name="data[Entry][school]" maxlength="50" id="EntrySchool" type="text" value="<?php if($entry_data['school']) echo $entry_data['school'] ?>"></div>

<!-- 
<div class="ui-select">
<div class="ui-btn ui-shadow ui-btn-corner-all ui-fullsize ui-btn-block ui-btn-icon-right ui-btn-up-c" data-mini="false" data-inline="false" data-theme="c" data-iconpos="right" data-icon="arrow-d" data-wrapperels="span" data-iconshadow="true" data-shadow="true" data-corners="true">
<select name="data[Entry][school_status]" id="EntrySchoolStatus">
<option value="1">在学中</option>
<option value="2">卒業</option>
</select>
</div></div>
 -->
<div class="input tel required">
<label class="ui-input-text" for="EntryTel">電話番号</label>
<input class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset" name="data[Entry][tel]" maxlength="50" id="EntryTel" required="required" type="tel" type="text" value="<?php if($entry_data['tel']) echo $entry_data['tel'] ?>"></div>

<div class="input email required">
<label class="ui-input-text" for="EntryEmail">メールアドレス</label>
<input class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset" name="data[Entry][email]" maxlength="100" id="EntryEmail" required="required" type="email" type="text" value="<?php if($entry_data['email']) echo $entry_data['email'] ?>">
</div>

<div class="input textarea">
<label class="ui-input-text" for="EntryDetail">その他</label>
<textarea class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset" name="data[Entry][detail]" cols="30" rows="6" id="EntryDetail"  type="text"><?php if($entry_data['detail']) echo $entry_data['detail'] ?></textarea>
</div>	
		 
	</fieldset>
	
<div class="false">
<input class="ui-btn-hidden" value="送信" type="submit"></div>
	
<?php echo $this->Form->end(); ?>
</div>