<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, user-scalable=no">
<title>お問い合せ・資料請求｜家庭教師＆個別指導の合格王</title>
<SCRIPT language="JavaScript1.2" src="../library/js/base.js"></SCRIPT>
<SCRIPT language="JavaScript1.2" src="../library/js/check_contact.js"></SCRIPT>
		<meta name="description" content="家庭教師なら家庭教師の合格王。家庭教師についてのお問い合せ・資料請求はこちらのフォームからどうぞ。">
		<meta name="Keywords" content="家庭教師, プロ家庭教師, 中学受験, インターネット家庭教師, お問い合せ, 資料, 資料請求, 質問">
<link href="../css/import.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/config.js"></script>
<script type="text/javascript" src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="../js/jquery.inc-6.js"></script>
</head>
<body id="b">
<div data-role="header" data-position="inline" data-theme="a"><a rel="external" data-ajax="false" href="javascript:history.back();" data-theme="a" data-corners="false">back</a><h1></h1></div>

<div data-role="header" data-theme="a"><p>お問い合わせ・資料請求</p></div>

<div id="container">

<p>以下のフォームにご記入のうえ、「確認」ボタンを押してください。</p>

<!--■ここからがコンテンツ■-->

<form method="post" action="mail.php">
                <div id="checkboxes1" data-role="fieldcontain">
                    <fieldset data-role="controlgroup" data-type="vertical">
                        <legend>
                            お問い合せ内容
                        </legend>
                        <input id="checkbox1" name="家庭教師についての資料のご請求" type="checkbox" />
                        <label for="checkbox1">
                            家庭教師についての資料のご請求
                        </label>
                        <input id="checkbox2" name="家庭教師の無料カウンセリング・無料体験レッスンの希望" type="checkbox" />
                        <label for="checkbox2">
                            家庭教師の無料カウンセリング・無料体験レッスンの希望
                        </label>
                        <input id="checkbox3" name="インターネット家庭教師についての資料のご請求" type="checkbox" />
                        <label for="checkbox3">
                            インターネット家庭教師についての資料のご請求
                        </label>
                        <input id="checkbox4" name="インターネット家庭教師の無料カウンセリング・無料体験レッスンの希望" type="checkbox" />
                        <label for="checkbox4">
                            インターネット家庭教師の無料カウンセリング・無料体験レッスンの希望
                        </label>
                        <input id="checkbox5" name="特別コースについて" type="checkbox" />
                        <label for="checkbox5">
                            特別コースについて
                        </label>
                        <input id="checkbox6" name="合格王に関するお問い合わせ" type="checkbox" />
                        <label for="checkbox6">
                            合格王に関するお問い合わせ
                        </label>
                        <input id="checkbox7" name="その他" type="checkbox" />
                        <label for="checkbox7">
                            その他
                        </label>
                    </fieldset>
                </div>
                <div data-role="fieldcontain">
                    <fieldset data-role="controlgroup">
                        <legend>
                            ご本人様のお名前
                        </legend>
                        <label for="name01">
                            漢字
                        </label>
                        <input name="ご本人様のお名前｜漢字" id="name01" type="text" />
                        <label for="name02">
                            かな
                        </label>
                        <input name="ご本人様のお名前｜かな" id="name02" type="text" />
                    </fieldset>
                </div>
                <div data-role="fieldcontain">
                    <fieldset data-role="controlgroup">
                        <legend>
                            保護者の方のお名前
                        </legend>
                        <label for="name03">
                            漢字
                        </label>
                        <input name="保護者の方のお名前｜漢字" id="name03" type="text" />
                        <label for="name04">
                            かな
                        </label>
                        <input name="保護者の方のお名前｜かな" id="name04" type="text" />
                    </fieldset>
                </div>
                <div data-role="fieldcontain">
                    <label for="school">
                        学年
                    </label>
                    <select name="学年" id="school">
                    <option value="" selected="selected">学年選択</option>
                    <option value="小学1年">小学1年</option>
                    <option value="小学2年">小学2年</option>
                    <option value="小学3年">小学3年</option>
                    <option value="小学4年">小学4年</option>
                    <option value="小学5年">小学5年</option>
                    <option value="小学6年">小学6年</option>
                    <option value="中学1年">中学1年</option>
                    <option value="中学2年">中学2年</option>
                    <option value="中学3年">中学3年</option>
                    <option value="高校1年">高校1年</option>
                    <option value="高校2年">高校2年</option>
                    <option value="高校3年">高校3年</option>
                    <option value="その他">その他</option>
                    </select>
                </div>
                <div data-role="fieldcontain">
                    <label for="sex">
                        性別
                    </label>
                    <select name="性別" id="sex">
                    <option value="" selected="selected">性別選択</option>
                    <option value="男性">男性</option>
                    <option value="女性">女性</option>
                    </select>
                </div>
                <div data-role="fieldcontain">
                    <fieldset data-role="controlgroup">
                        <label for="zip01" style="float:left">
                            住所
                        </label>
                        <div style="clear:left">〒</div><input name="〒1" type="text" id="zip01" size="3" maxlength="3" pattern="[0-9]*" style="width:60px; float:left">
                        <input name="〒2" type="text" id="zip02" size="4" maxlength="4" pattern="[0-9]*" style="width:80px; float:left">
                    </fieldset>
<select name="都道府県" id="add01">
<option value="" selected="selected">都道府県</option>
<option value="北海道">北海道</option>
<option value="青森県">青森県</option>
<option value="岩手県">岩手県</option>
<option value="宮城県">宮城県</option>
<option value="秋田県">秋田県</option>
<option value="山形県">山形県</option>
<option value="福島県">福島県</option>
<option value="茨城県">茨城県</option>
<option value="栃木県">栃木県</option>
<option value="群馬県">群馬県</option>
<option value="埼玉県">埼玉県</option>
<option value="千葉県">千葉県</option>
<option value="東京都">東京都</option>
<option value="神奈川県">神奈川県</option>
<option value="新潟県">新潟県</option>
<option value="富山県">富山県</option>
<option value="石川県">石川県</option>
<option value="福井県">福井県</option>
<option value="山梨県">山梨県</option>
<option value="長野県">長野県</option>
<option value="岐阜県">岐阜県</option>
<option value="静岡県">静岡県</option>
<option value="愛知県">愛知県</option>
<option value="三重県">三重県</option>
<option value="滋賀県">滋賀県</option>
<option value="京都府">京都府</option>
<option value="大阪府">大阪府</option>
<option value="兵庫県">兵庫県</option>
<option value="奈良県">奈良県</option>
<option value="和歌山県">和歌山県</option>
<option value="鳥取県">鳥取県</option>
<option value="島根県">島根県</option>
<option value="岡山県">岡山県</option>
<option value="広島県">広島県</option>
<option value="山口県">山口県</option>
<option value="徳島県">徳島県</option>
<option value="香川県">香川県</option>
<option value="愛媛県">愛媛県</option>
<option value="高知県">高知県</option>
<option value="福岡県">福岡県</option>
<option value="佐賀県">佐賀県</option>
<option value="長崎県">長崎県</option>
<option value="熊本県">熊本県</option>
<option value="大分県">大分県</option>
<option value="宮崎県">宮崎県</option>
<option value="鹿児島県">鹿児島県</option>
<option value="沖縄県">沖縄県</option>
</select>
<input name="住所" type="text" id="add02" size="34">
                </div>
                <div data-role="fieldcontain">
                    <fieldset data-role="controlgroup">
                        <label for="tel" style="float:left">
                            電話番号
                        </label>
                        <div style="clear:left">
                        <input name="市外局番" type="text" id="tel01" size="4" maxlength="4" pattern="[0-9]*" style="width:80px; float:left">
                        <input name="電話番号1" type="text" id="tel02" size="4" maxlength="4" pattern="[0-9]*" style="width:80px; float:left">
                        <input name="電話番号2" type="text" id="tel03" size="4" maxlength="4" pattern="[0-9]*" style="width:80px; float:left">
                        </div>
                    </fieldset>
                </div>
                <div data-role="fieldcontain">
                    <fieldset data-role="controlgroup">
                        <label for="mail" style="float:left">
                            メールアドレス
                        </label>
                        <input name="メールアドレス" type="text" id="mail" size="34" style="clear:left">
                    </fieldset>
                </div>
                <div data-role="fieldcontain">
                    <fieldset data-role="controlgroup">
                        <label for="comment">
                            質問・要望など
                        </label>
                        <textarea name="質問・要望など" id="comment"></textarea>
                    </fieldset>
                </div>



<div style="text-align:center"><input type="submit" data-theme="a" value="　確 認　" data-inline="true" /><input type="reset" value="リセット" data-theme="a" data-inline="true" /></div>
</form>



<script language="JavaScript" type="text/JavaScript">
<!--
function addBookmark(title,url) {
if (window.sidebar) {
window.sidebar.addPanel(title, url,"");
} else if( document.all ) {
window.external.AddFavorite( url, title);
} else if( window.opera && window.print ) {
return true;
}
}
//-->
</script>

<br />

		<a rel="external" data-ajax="false" href="tel:0120041590">
		<div class="maxw"><img src="../img/banner-footer02.png" alt="0120-041-590"></div>
		</a>
<ul data-role="listview" data-theme="d">
	<li><a rel="external" data-ajax="false" href="../area.html"><h3>対応エリア</h3></a></li>
	<li><a rel="external" data-ajax="false" href="../profile.html"><h3>会社案内</h3></a></li>
	<li><a rel="external" data-ajax="false" href="../policy.html"><h3>プライバシーポリシー</h3></a></li>
</ul>
	<!--</div>--><!-- /content -->
<div style="clear: both;"></div>
<div data-role="footer" data-theme="c"><p class="center">&copy;Copyright Gokakuoh Inc. All rights reserved.</p></div>
</div><!-- /container -->
</body>
</html>
