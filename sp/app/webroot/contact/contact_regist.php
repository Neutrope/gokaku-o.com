<?php
	if ($name01 == "") {
		echo "<META http-equiv=\"refresh\" content=\"0;URL=./index.php\">";
	} else {
		$tel = $tel01 . "-" . $tel02 . "-" . $tel03;
		$kind = implode( "\n" , $kind );
		if ( $comment == "") { $comment = "記入なし"; }
		$comment = str_replace("<BR>","\n\r",$comment);

		require_once("../library/include/jcode.phps");
	//	$to = "office@freakdesign.jp";
		$to = "info2@gokaku-o.com";
		$from = $mail;
		$_subject_a = "【資料請求・お問い合わせ】";
		$_subject_b = "【合格王】資料請求・お問い合わせありがとうございました";

		$body00a.=
		"\n━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━" .
		"\n資料請求・お問い合わせが届きました。" .
		"\n━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━" .
		"\n" .
		"\n";

		$body00b.=
		"\n━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━" .
		"\nお問い合わせを承りました。" .
		"\n━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━" .
		"\nお問い合わせ、ありがとうございました。" . 
		"\n後日、弊社担当者よりご連絡差し上げます。" . 
		"\n" .
		"\n" ;

		$body01.=
		"\n■お問い合わせ内容".
		"\n---------------------------------------------------------------" .
		"\n" . $kind .
		"\n---------------------------------------------------------------" .
		"\n" .
		"\n" .
		"\n■個人情報".
		"\n---------------------------------------------------------------" .
		"\nお名前：" . $name01 . "（" . $name02 . "）" . "様".
		"\n保護者：" . $name03 . "（" . $name04 . "）" . "様".
		"\n学年：" . $school . 
		"\n性別：" . $sex .  
		"\n〒" . $zip01 . "-" . $zip02 . " " . $add01 . $add02 . 
		"\nTEL：" . $tel .
		"\nアドレス：mailto:" . $mail .
		"\n---------------------------------------------------------------" .
		"\n" .
		"\n" .
		"\n■質問・要望など" .
		"\n---------------------------------------------------------------" .
		"\n" . $comment .  
		"\n---------------------------------------------------------------" .
		"\n" .
		"\n";

		$body02.=
		"\n━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━" .
		"\n株式会社合格王システムズ" .
		"\n〒169-0075　東京都新宿区高田馬場4-4-18　NTビル" .
		"\nTEL:0120-041-590" .
		"\nURL:http://www.gokaku-o.com/" .
		"\n━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━";
	
		$subject_a="=?iso-2002-jp?B?".base64_encode(jcodeconvert($_subject_a,0,3))."?=";
		$subject_b="=?iso-2002-jp?B?".base64_encode(jcodeconvert($_subject_b,0,3))."?=";

		$body_a= $body00a . $body01 . $body02;
		$body_a =jcodeconvert($body_a,0,3);
		$header_a  ="Content-type: text/plain; charset=ISO-2022-JP\r\n";
		$header_a .="content-transfer-encoding: 7bit\r\n";
		$header_a .="From: GOKAKU-O.COM <info2@gokaku-o.com>\r\n";
		$header_a .="Reply-To: ".$mail."\nX-Mailer: PHP/".phpversion();


		$body_b= $body00b . $body01 . $body02;
		$body_b =jcodeconvert($body_b,0,3);
		$header_b  ="Content-type: text/plain; charset=ISO-2022-JP\r\n";
		$header_b .="Content-transfer-encoding: 7bit\r\n";
		$header_b .="From: GOKAKU-O.COM <info2@gokaku-o.com>\r\n";
		$header_b .="Reply-To: info2@gokaku-o.com" . "\nX-Mailer: PHP/".phpversion();

		mail ($to, $subject_a, $body_a, $header_a)&&mail ($from, $subject_b, $body_b, $header_b);

	
		$kind = str_replace("\n","<BR>",$kind);

		$comment = str_replace(",","",$comment);
		$comment = str_replace(" ","",$comment);
		$comment = str_replace("\r","",$comment);
		$comment = str_replace("\n","<BR>",$comment);
		
		$logfile = "../../data/log_contact.csv";

		$txt = implode(",", array( gmdate("Y/m/d H:i:s",time()+9*3600) , @gethostbyaddr($REMOTE_ADDR) , $kind , $name01 , $name02 , $name03 , $name04 , $school , $sex , $zip01 ,$zip02 , $add01 , $add02 , $tel , $mail , $comment));
		$fp = fopen ($logfile, "a");
		flock($fp ,LOCK_EX);
		fputs($fp, "$txt\n");
		flock($fp ,LOCK_UN);
		fclose ($fp);	
	}
?><!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, user-scalable=no">
<title>お問い合せ・資料請求｜家庭教師＆個別指導の合格王</title>
<SCRIPT language="JavaScript1.2" src="../library/js/base.js"></SCRIPT>
		<meta name="description" content="家庭教師なら家庭教師の合格王。家庭教師についてのお問い合せ・資料請求はこちらのフォームからどうぞ。">
		<meta name="Keywords" content="家庭教師, プロ家庭教師, 中学受験, インターネット家庭教師, お問い合せ, 資料, 資料請求, 質問">
<link href="../css/import.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/config.js"></script>
<script type="text/javascript" src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="../js/jquery.inc-6.js"></script>
</head>
<body id="b">
<div data-role="header" data-position="inline" data-theme="a"><a rel="external" data-ajax="false" href="javascript:history.back();" data-theme="a" data-corners="false">back</a><h1></h1></div>

<div data-role="header" data-theme="a"><p>お問い合わせ・資料請求</p></div>

<div id="container">

<p>お問い合わせ、ありがとうございました。<br />後日、弊社担当者よりご連絡差し上げます。</p>

<!--■ここからがコンテンツ■-->

<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
<TR>
<TD background="/library/images/common/co_bar.gif"><IMG src="/library/images/common/co_spacer.gif" width="1" height="1"></TD>
</TR>
<TR>
<TD>
<TABLE border="0" align="left" cellpadding="0" cellspacing="10">
<TR>
<TD width="100" class="txt12 font_Bold">お問い合わせ<BR>
内容</TD>
<TD class="txt12"><?php
if ( $kind != "" ) { 
	echo $kind;
} else { echo "記入なし"; }
?>
</TD>
</TR>
</TABLE>
</TD>
</TR>
<TR>
<TD><TABLE border="0" align="left" cellpadding="0" cellspacing="10">
<TR>
<TD width="100" class="txt12 font_Bold">ご本人様の<BR>
お名前</TD>
<TD class="txt12"><?php echo $name01 . "（" . $name02 .  "）様"; ?></TD>
</TR>
</TABLE>
</TD>
</TR>
<TR>
<TD><TABLE border="0" align="left" cellpadding="0" cellspacing="10">
<TR>
<TD width="100" class="txt12 font_Bold">保護者の方の<BR>
お名前</TD>
<TD class="txt12"><?php echo $name03 . "（" . $name04 .  "）様"; ?></TD>
</TR>
</TABLE>
</TD>
</TR>
<TR>
<TD><TABLE border="0" align="left" cellpadding="0" cellspacing="10">
<TR>
<TD width="100" class="txt12 font_Bold">学年<SPAN class="txt10"></SPAN></TD>
<TD class="txt12"><?php if ( $school != "" ) { echo $school; } else { echo "選択なし"; } ?>
</TD>
</TR>
</TABLE>
</TD>
</TR>
<TR>
<TD><TABLE border="0" align="left" cellpadding="0" cellspacing="10">
<TR>
<TD width="100" class="txt12 font_Bold">性別<SPAN class="txt10"></SPAN></TD>
<TD class="txt12"><?php if ( $sex != "" ) { echo $sex; } else { echo "選択なし"; } ?>
</TD>
</TR>
</TABLE>
</TD>
</TR>
<TR>
<TD><TABLE border="0" align="left" cellpadding="0" cellspacing="10">
<TR>
<TD width="100" class="txt12 font_Bold">住所</TD>
<TD class="txt12"><?php echo "〒" . $zip01 . "-" . $zip02 . "<BR>" . $add01 . $add02; ?> </TD>
</TR>
</TABLE>
</TD>
</TR>
<TR>
<TD><TABLE border="0" align="left" cellpadding="0" cellspacing="10">
<TR>
<TD width="100" class="txt12 font_Bold">電話番号</TD>
<TD class="txt12"><?php if ( $tel01 != "" ) { echo $tel01 . "-" . $tel02 . "-" . $tel03; } else { echo "記入なし"; } ?>
</TD>
</TR>
</TABLE>
</TD>
</TR>
<TR>
<TD><TABLE border="0" align="left" cellpadding="0" cellspacing="10">
<TR>
<TD width="100" class="txt12 font_Bold">メールアドレス</TD>
<TD class="txt12"><?php echo $mail; ?> </TD>
</TR>
</TABLE>
</TD>
</TR>
<TR>
<TD>
<TABLE width="100%" border="0" cellpadding="0" cellspacing="10">
<TR>
<TD width="100" class="txt12 font_Bold">質問・要望など</TD>
<TD class="txt12"><?php
	$comment = str_replace(" ","",$comment);
	$comment = str_replace(",","",$comment);
	$comment = str_replace("\r","",$comment);
	$comment = str_replace("\n","<BR>",$comment);
	echo ($comment); 
?>
</TD>
</TR>
</TABLE>
</TD>
</TR>
</TABLE>

<!--■■WIN5様アフェリ用タグ■■-->
<img src="http://win5.biz/sys/track.php?t=gtp5k3N8&p=507699d8adf7b&guid=ON" width="1" height="1">
</div>


<script language="JavaScript" type="text/JavaScript">
<!--
function addBookmark(title,url) {
if (window.sidebar) {
window.sidebar.addPanel(title, url,"");
} else if( document.all ) {
window.external.AddFavorite( url, title);
} else if( window.opera && window.print ) {
return true;
}
}
//-->
</script>


<!-- EBiS tag version2.10 start -->

<script type="text/javascript">

<!--

if ( location.protocol == 'http:' ){

    strServerName = 'http://ac.ebis.ne.jp';

} else {

    strServerName = 'https://ac.ebis.ne.jp';

}

cid = 'PdPJ39us'; pid = 'e-matome1'; m1id=''; a1id=''; o1id=''; o2id=''; o3id=''; o4id=''; o5id='';

document.write("<scr" + "ipt type=\"text\/javascript\" src=\"" + strServerName + "\/ebis_tag.php?cid=" + cid + "&pid=" + pid + "&m1id=" + m1id +

"&a1id=" + a1id + "&o1id=" + o1id + "&o2id=" + o2id + "&o3id=" + o3id + "&o4id=" + o4id + "&o5id=" + o5id + "\"><\/scr" + "ipt>");

// -->

</script>

<noscript>

<img src="https://ac.ebis.ne.jp/log.php?argument=PdPJ39us&ebisPageID=e-matome1&ebisMember=&ebisAmount=&ebisOther1=&ebisOther2=&ebisOther3=&ebisOther4=&ebisOther5=" width="0" height="0">

</noscript>

<!-- EBiS tag end -->

</body>
</html>
