<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, user-scalable=no">
<title>お問い合せ・資料請求｜家庭教師＆個別指導の合格王</title>
<SCRIPT language="JavaScript1.2" src="../library/js/base.js"></SCRIPT>
<SCRIPT language="JavaScript1.2" src="../library/js/check_contact.js"></SCRIPT>
		<meta name="description" content="家庭教師なら家庭教師の合格王。家庭教師についてのお問い合せ・資料請求はこちらのフォームからどうぞ。">
		<meta name="Keywords" content="家庭教師, プロ家庭教師, 中学受験, インターネット家庭教師, お問い合せ, 資料, 資料請求, 質問">
<link href="../css/import.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/config.js"></script>
<script type="text/javascript" src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="../js/jquery.inc-6.js"></script>
</head>
<body id="b">
<div data-role="header" data-position="inline" data-theme="a"><a rel="external" data-ajax="false" href="javascript:history.back();" data-theme="a" data-corners="false">back</a><h1></h1></div>

<div data-role="header" data-theme="a"><p>お問い合わせ・資料請求</p></div>

<div id="container">

<p>以下の入力内容をご確認のうえ、「送信」ボタンを押してください。<br />修正する場合は、「修正」ボタンを押してください。</p>

<!--■ここからがコンテンツ■-->

<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
<?php
if (get_magic_quotes_gpc()) {
//	$kind			= stripslashes($kind);
	$name01	= stripslashes($name01);
	$name02	= stripslashes($name02);
	$name03	= stripslashes($name03);
	$name04	= stripslashes($name04);
	$school		= stripslashes($school);
	$sex			= stripslashes($sex);
	$zip01		= stripslashes($zip01);
	$zip02		= stripslashes($zip02);
	$add01		= stripslashes($add01);
	$add02		= stripslashes($add02);
	$tel01		= stripslashes($tel01);
	$tel02		= stripslashes($tel02);
	$tel03		= stripslashes($tel03);
	$mail			= stripslashes($mail);
	$comment	= stripslashes($comment);
}
?>
<TR>
<TD background="/library/images/common/co_bar.gif"><IMG src="/library/images/common/co_spacer.gif" width="1" height="1"></TD>
</TR>
<TR>
<TD>
<TABLE border="0" align="left" cellpadding="0" cellspacing="10">
<TR>
<TD width="100" class="txt12 font_Bold">お問い合わせ<BR>
内容</TD>
<TD class="txt12"><?php for( $i=0; $i < count($kind); ++$i ) { echo ( $kind[$i] . "<BR>"); } ?></TD>
</TR>
</TABLE>
</TD>
</TR>
<TR>
<TD><TABLE border="0" align="left" cellpadding="0" cellspacing="10">
<TR>
<TD width="100" class="txt12 font_Bold">ご本人様の<BR>
お名前</TD>
<TD class="txt12"><?php echo $name01 . "（" . $name02 .  "）様"; ?></TD>
</TR>
</TABLE>
</TD>
</TR>
<TR>
<TD><TABLE border="0" align="left" cellpadding="0" cellspacing="10">
<TR>
<TD width="100" class="txt12 font_Bold">保護者の方の<BR>
お名前</TD>
<TD class="txt12"><?php echo $name03 . "（" . $name04 .  "）様"; ?></TD>
</TR>
</TABLE>
</TD>
</TR>
<TR>
<TD><TABLE border="0" align="left" cellpadding="0" cellspacing="10">
<TR>
<TD width="100" class="txt12 font_Bold">学年<SPAN class="txt10"></SPAN></TD>
<TD class="txt12"><?php if ( $school != "" ) { echo $school; } else { echo "選択なし"; } ?>
</TD>
</TR>
</TABLE>
</TD>
</TR>
<TR>
<TD><TABLE border="0" align="left" cellpadding="0" cellspacing="10">
<TR>
<TD width="100" class="txt12 font_Bold">性別<SPAN class="txt10"></SPAN></TD>
<TD class="txt12"><?php if ( $sex != "" ) { echo $sex; } else { echo "選択なし"; } ?>
</TD>
</TR>
</TABLE>
</TD>
</TR>
<TR>
<TD><TABLE border="0" align="left" cellpadding="0" cellspacing="10">
<TR>
<TD width="100" class="txt12 font_Bold">住所</TD>
<TD class="txt12"><?php echo "〒" . $zip01 . "-" . $zip02 . "<BR>" . $add01 . $add02; ?>
</TD>
</TR>
</TABLE>
</TD>
</TR>
<TR>
<TD><TABLE border="0" align="left" cellpadding="0" cellspacing="10">
<TR>
<TD width="100" class="txt12 font_Bold">電話番号</TD>
<TD class="txt12"><?php if ( $tel01 != "" ) { echo $tel01 . "-" . $tel02 . "-" . $tel03; } else { echo "記入なし"; } ?>
</TD>
</TR>
</TABLE>
</TD>
</TR>
<TR>
<TD><TABLE border="0" align="left" cellpadding="0" cellspacing="10">
<TR>
<TD width="100" class="txt12 font_Bold">メールアドレス</TD>
<TD class="txt12"><?php echo $mail; ?>
</TD>
</TR>
</TABLE>
</TD>
</TR>
<TR>
<TD><TABLE width="575" border="0" cellpadding="0" cellspacing="10">
<TR>
<TD width="100" class="txt12 font_Bold">質問・要望など</TD>
<TD width="450" class="txt12"><?php
	$comment = str_replace(" ","",$comment);
	$comment = str_replace(",","",$comment);
	$comment = str_replace("\r","",$comment);
	$comment = str_replace("\n","<BR>",$comment);
	echo ($comment); 
?>
</TD>
</TR>
</TABLE>
</TD>
</TR>
<TR>
<TD height="30" align="center" valign="bottom"><TABLE border="0" cellspacing="0" cellpadding="0">
<TR>
<FORM name="form" action="contact_regist.php" method="POST">
<TD><?php
	for ($i=0; $i < count($kind); ++$i ) { echo ("<INPUT type=\"hidden\" name=\"kind[$i]\" value=\"$kind[$i]\">\n");} 

	echo ("<INPUT type=\"hidden\" name=\"name01\" value=\"$name01\">\n");
	echo ("<INPUT type=\"hidden\" name=\"name02\" value=\"$name02\">\n");
	echo ("<INPUT type=\"hidden\" name=\"name03\" value=\"$name03\">\n");
	echo ("<INPUT type=\"hidden\" name=\"name04\" value=\"$name04\">\n");
	echo ("<INPUT type=\"hidden\" name=\"school\" value=\"$school\">\n");
	echo ("<INPUT type=\"hidden\" name=\"sex\" value=\"$sex\">\n");
	echo ("<INPUT type=\"hidden\" name=\"zip01\" value=\"$zip01\">\n");
	echo ("<INPUT type=\"hidden\" name=\"zip02\" value=\"$zip02\">\n");
	echo ("<INPUT type=\"hidden\" name=\"add01\" value=\"$add01\">\n");
	echo ("<INPUT type=\"hidden\" name=\"add02\" value=\"$add02\">\n");
	echo ("<INPUT type=\"hidden\" name=\"tel01\" value=\"$tel01\">\n");
	echo ("<INPUT type=\"hidden\" name=\"tel02\" value=\"$tel02\">\n");
	echo ("<INPUT type=\"hidden\" name=\"tel03\" value=\"$tel03\">\n");
	echo ("<INPUT type=\"hidden\" name=\"mail\" value=\"$mail\">\n");
	echo ("<INPUT type=\"hidden\" name=\"comment\" value=\"$comment\">\n");
?>
<input type="submit" name="Submit" data-theme="a" value="送信" data-inline="true" />
</TD>
</FORM>
<TD>&nbsp;&nbsp;&nbsp;</TD>
<FORM name="form" action="index.php" method="POST">
<TD><?php
	for ($i=0; $i < count($kind); ++$i ) { echo ("<INPUT type=\"hidden\" name=\"kind[$i]\" value=\"$kind[$i]\">\n");} 

	echo ("<INPUT type=\"hidden\" name=\"name01\" value=\"$name01\">\n");
	echo ("<INPUT type=\"hidden\" name=\"name02\" value=\"$name02\">\n");
	echo ("<INPUT type=\"hidden\" name=\"name03\" value=\"$name03\">\n");
	echo ("<INPUT type=\"hidden\" name=\"name04\" value=\"$name04\">\n");
	echo ("<INPUT type=\"hidden\" name=\"school\" value=\"$school\">\n");
	echo ("<INPUT type=\"hidden\" name=\"sex\" value=\"$sex\">\n");
	echo ("<INPUT type=\"hidden\" name=\"zip01\" value=\"$zip01\">\n");
	echo ("<INPUT type=\"hidden\" name=\"zip02\" value=\"$zip02\">\n");
	echo ("<INPUT type=\"hidden\" name=\"add01\" value=\"$add01\">\n");
	echo ("<INPUT type=\"hidden\" name=\"add02\" value=\"$add02\">\n");
	echo ("<INPUT type=\"hidden\" name=\"tel01\" value=\"$tel01\">\n");
	echo ("<INPUT type=\"hidden\" name=\"tel02\" value=\"$tel02\">\n");
	echo ("<INPUT type=\"hidden\" name=\"tel03\" value=\"$tel03\">\n");
	echo ("<INPUT type=\"hidden\" name=\"mail\" value=\"$mail\">\n");
	echo ("<INPUT type=\"hidden\" name=\"comment\" value=\"$comment\">\n");
?>
<input type="submit" name="Submit" data-theme="a" value="修正" data-inline="true" />
</TD>
</FORM>
</TR>
</TABLE>
</TD>
</TR>
</TABLE>

</div>



<script language="JavaScript" type="text/JavaScript">
<!--
function addBookmark(title,url) {
if (window.sidebar) {
window.sidebar.addPanel(title, url,"");
} else if( document.all ) {
window.external.AddFavorite( url, title);
} else if( window.opera && window.print ) {
return true;
}
}
//-->
</script>

</body>
</html>
