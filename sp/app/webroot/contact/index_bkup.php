<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, user-scalable=no">
<title>お問い合せ・資料請求｜家庭教師＆個別指導の合格王</title>
<SCRIPT language="JavaScript1.2" src="../library/js/base.js"></SCRIPT>
<SCRIPT language="JavaScript1.2" src="../library/js/check_contact.js"></SCRIPT>
		<meta name="description" content="家庭教師なら家庭教師の合格王。家庭教師についてのお問い合せ・資料請求はこちらのフォームからどうぞ。">
		<meta name="Keywords" content="家庭教師, プロ家庭教師, 中学受験, インターネット家庭教師, お問い合せ, 資料, 資料請求, 質問">
<link href="../css/import.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/config.js"></script>
<script type="text/javascript" src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="../js/jquery.inc-6.js"></script>
</head>
<body id="b">
<div data-role="header" data-position="inline" data-theme="a"><a rel="external" data-ajax="false" href="javascript:history.back();" data-theme="a" data-corners="false">back</a><h1></h1></div>

<div data-role="header" data-theme="a"><p>お問い合わせ・資料請求</p></div>

<p>以下のフォームにご記入のうえ、「確認」ボタンを押してください。</p>

<!--■ここからがコンテンツ■-->

<form name="form" action="contact_conf.php" method="POST" onSubmit="return formcheck();">
                <div id="checkboxes1" data-role="fieldcontain">
                    <fieldset data-role="controlgroup" data-type="vertical">
                        <legend>
                            お問い合せ内容
                        </legend>
                        <input id="checkbox1" name="naiyou1" type="checkbox" />
                        <label for="checkbox1">
                            家庭教師についての資料のご請求
                        </label>
                        <input id="checkbox2" name="naiyou2" type="checkbox" />
                        <label for="checkbox2">
                            家庭教師の無料カウンセリング・無料体験レッスンの希望
                        </label>
                        <input id="checkbox3" name="naiyou3" type="checkbox" />
                        <label for="checkbox3">
                            インターネット家庭教師についての資料のご請求
                        </label>
                        <input id="checkbox4" name="naiyou4" type="checkbox" />
                        <label for="checkbox4">
                            インターネット家庭教師の無料カウンセリング・無料体験レッスンの希望
                        </label>
                        <input id="checkbox5" name="naiyou5" type="checkbox" />
                        <label for="checkbox5">
                            特別コースについて
                        </label>
                        <input id="checkbox6" name="naiyou6" type="checkbox" />
                        <label for="checkbox6">
                            合格王に関するお問い合わせ
                        </label>
                        <input id="checkbox7" name="naiyou7" type="checkbox" />
                        <label for="checkbox7">
                            その他
                        </label>
                    </fieldset>
                </div>
                <div data-role="fieldcontain">
                    <fieldset data-role="controlgroup">
                        <legend>
                            ご本人様のお名前
                        </legend>
                        <label for="name01">
                            漢字
                        </label>
                        <input name="name01" id="name01" placeholder="" value="<?php if ( $name01 !="" ) { echo $name01; } ?>" type="text" />
                        <label for="name02">
                            かな
                        </label>
                        <input name="name02" id="name02" placeholder="" value="<?php if ( $name02 !="" ) { echo $name02; } ?>" type="text" />
                    </fieldset>
                </div>
                <div data-role="fieldcontain">
                    <fieldset data-role="controlgroup">
                        <legend>
                            保護者の方のお名前
                        </legend>
                        <label for="name03">
                            漢字
                        </label>
                        <input name="name03" id="name03" placeholder="" value="<?php if ( $name03 !="" ) { echo $name03; } ?>" type="text" />
                        <label for="name04">
                            かな
                        </label>
                        <input name="name04" id="name04" placeholder="" value="<?php if ( $name04 !="" ) { echo $name04; } ?>" type="text" />
                    </fieldset>
                </div>
                <div data-role="fieldcontain">
                    <label for="school">
                        学年
                    </label>
                    <select name="school" id="school">
                    <option value="" <?php if ( $school =="" ) { echo "selected"; } ?>>学年選択</option>
                    <option value="小学1年" <?php if ( $school =="小学1年" ) { echo "selected"; } ?>>小学1年</option>
                    <option value="小学2年" <?php if ( $school =="小学2年" ) { echo "selected"; } ?>>小学2年</option>
                    <option value="小学3年" <?php if ( $school =="小学3年" ) { echo "selected"; } ?>>小学3年</option>
                    <option value="小学4年" <?php if ( $school =="小学4年" ) { echo "selected"; } ?>>小学4年</option>
                    <option value="小学5年" <?php if ( $school =="小学5年" ) { echo "selected"; } ?>>小学5年</option>
                    <option value="小学6年" <?php if ( $school =="小学6年" ) { echo "selected"; } ?>>小学6年</option>
                    <option value="中学1年" <?php if ( $school =="中学1年" ) { echo "selected"; } ?>>中学1年</option>
                    <option value="中学2年" <?php if ( $school =="中学2年" ) { echo "selected"; } ?>>中学2年</option>
                    <option value="中学3年" <?php if ( $school =="中学3年" ) { echo "selected"; } ?>>中学3年</option>
                    <option value="高校1年" <?php if ( $school =="高校1年" ) { echo "selected"; } ?>>高校1年</option>
                    <option value="高校2年" <?php if ( $school =="高校2年" ) { echo "selected"; } ?>>高校2年</option>
                    <option value="高校3年" <?php if ( $school =="高校3年" ) { echo "selected"; } ?>>高校3年</option>
                    <option value="その他" <?php if ( $school =="その他" ) { echo "selected"; } ?>>その他</option>
                    </select>
                </div>
                <div data-role="fieldcontain">
                    <label for="sex">
                        性別
                    </label>
                    <select name="sex" id="sex">
                    <option value="" <?php if ( $sex =="" ) { echo "selected"; } ?>>性別選択</option>
                    <option value="男性" <?php if ( $sex =="男性" ) { echo "selected"; } ?>>男性</option>
                    <option value="女性" <?php if ( $sex =="女性" ) { echo "selected"; } ?>>女性</option>
                    </select>
                </div>
                <div data-role="fieldcontain">
                    <fieldset data-role="controlgroup">
                        <label for="zip01">
                            住所
                        </label>
                        <div style="float:left">〒</div><input name="zip01" type="text" id="zip01" size="3" maxlength="3" value="<?php if ( $zip01 !="" ) { echo $zip01; } ?>" style="width:60px; float:left">
                        <input name="zip02" type="text" id="zip02" size="4" maxlength="4" value="<?php if ( $zip02 !="" ) { echo $zip02; } ?>" style="width:80px; float:left">
                    </fieldset>
<select name="add01" id="add01">
<option value="" <?php if ( $add01 =="" ) { echo "selected"; } ?>>都道府県</option>
<option value="北海道" <?php if ( $add01 =="北海道" ) { echo "selected"; } ?>>北海道</option>
<option value="青森県" <?php if ( $add01 =="青森県" ) { echo "selected"; } ?>>青森県</option>
<option value="岩手県" <?php if ( $add01 =="岩手県" ) { echo "selected"; } ?>>岩手県</option>
<option value="宮城県" <?php if ( $add01 =="宮城県" ) { echo "selected"; } ?>>宮城県</option>
<option value="秋田県" <?php if ( $add01 =="秋田県" ) { echo "selected"; } ?>>秋田県</option>
<option value="山形県" <?php if ( $add01 =="山形県" ) { echo "selected"; } ?>>山形県</option>
<option value="福島県" <?php if ( $add01 =="福島県" ) { echo "selected"; } ?>>福島県</option>
<option value="茨城県" <?php if ( $add01 =="茨城県" ) { echo "selected"; } ?>>茨城県</option>
<option value="栃木県" <?php if ( $add01 =="栃木県" ) { echo "selected"; } ?>>栃木県</option>
<option value="群馬県" <?php if ( $add01 =="群馬県" ) { echo "selected"; } ?>>群馬県</option>
<option value="埼玉県" <?php if ( $add01 =="埼玉県" ) { echo "selected"; } ?>>埼玉県</option>
<option value="千葉県" <?php if ( $add01 =="千葉県" ) { echo "selected"; } ?>>千葉県</option>
<option value="東京都" <?php if ( $add01 =="東京都" ) { echo "selected"; } ?>>東京都</option>
<option value="神奈川県" <?php if ( $add01 =="神奈川県" ) { echo "selected"; } ?>>神奈川県</option>
<option value="新潟県" <?php if ( $add01 =="新潟県" ) { echo "selected"; } ?>>新潟県</option>
<option value="富山県" <?php if ( $add01 =="富山県" ) { echo "selected"; } ?>>富山県</option>
<option value="石川県" <?php if ( $add01 =="石川県" ) { echo "selected"; } ?>>石川県</option>
<option value="福井県" <?php if ( $add01 =="福井県" ) { echo "selected"; } ?>>福井県</option>
<option value="山梨県" <?php if ( $add01 =="山梨県" ) { echo "selected"; } ?>>山梨県</option>
<option value="長野県" <?php if ( $add01 =="長野県" ) { echo "selected"; } ?>>長野県</option>
<option value="岐阜県" <?php if ( $add01 =="岐阜県" ) { echo "selected"; } ?>>岐阜県</option>
<option value="静岡県" <?php if ( $add01 =="静岡県" ) { echo "selected"; } ?>>静岡県</option>
<option value="愛知県" <?php if ( $add01 =="愛知県" ) { echo "selected"; } ?>>愛知県</option>
<option value="三重県" <?php if ( $add01 =="三重県" ) { echo "selected"; } ?>>三重県</option>
<option value="滋賀県" <?php if ( $add01 =="滋賀県" ) { echo "selected"; } ?>>滋賀県</option>
<option value="京都府" <?php if ( $add01 =="京都府" ) { echo "selected"; } ?>>京都府</option>
<option value="大阪府" <?php if ( $add01 =="大阪府" ) { echo "selected"; } ?>>大阪府</option>
<option value="兵庫県" <?php if ( $add01 =="兵庫県" ) { echo "selected"; } ?>>兵庫県</option>
<option value="奈良県" <?php if ( $add01 =="奈良県" ) { echo "selected"; } ?>>奈良県</option>
<option value="和歌山県" <?php if ( $add01 =="和歌山県" ) { echo "selected"; } ?>>和歌山県</option>
<option value="鳥取県" <?php if ( $add01 =="鳥取県" ) { echo "selected"; } ?>>鳥取県</option>
<option value="島根県" <?php if ( $add01 =="島根県" ) { echo "selected"; } ?>>島根県</option>
<option value="岡山県" <?php if ( $add01 =="岡山県" ) { echo "selected"; } ?>>岡山県</option>
<option value="広島県" <?php if ( $add01 =="広島県" ) { echo "selected"; } ?>>広島県</option>
<option value="山口県" <?php if ( $add01 =="山口県" ) { echo "selected"; } ?>>山口県</option>
<option value="徳島県" <?php if ( $add01 =="徳島県" ) { echo "selected"; } ?>>徳島県</option>
<option value="香川県" <?php if ( $add01 =="香川県" ) { echo "selected"; } ?>>香川県</option>
<option value="愛媛県" <?php if ( $add01 =="愛媛県" ) { echo "selected"; } ?>>愛媛県</option>
<option value="高知県" <?php if ( $add01 =="高知県" ) { echo "selected"; } ?>>高知県</option>
<option value="福岡県" <?php if ( $add01 =="福岡県" ) { echo "selected"; } ?>>福岡県</option>
<option value="佐賀県" <?php if ( $add01 =="佐賀県" ) { echo "selected"; } ?>>佐賀県</option>
<option value="長崎県" <?php if ( $add01 =="長崎県" ) { echo "selected"; } ?>>長崎県</option>
<option value="熊本県" <?php if ( $add01 =="熊本県" ) { echo "selected"; } ?>>熊本県</option>
<option value="大分県" <?php if ( $add01 =="大分県" ) { echo "selected"; } ?>>大分県</option>
<option value="宮崎県" <?php if ( $add01 =="宮崎県" ) { echo "selected"; } ?>>宮崎県</option>
<option value="鹿児島県" <?php if ( $add01 =="鹿児島県" ) { echo "selected"; } ?>>鹿児島県</option>
<option value="沖縄県" <?php if ( $add01 =="沖縄県" ) { echo "selected"; } ?>>沖縄県</option>
</select>
<input name="add02" type="text" id="add02" size="34" value="<?php if ( $add02 !="" ) { echo $add02; } ?>">
                </div>
                <div data-role="fieldcontain">
                    <fieldset data-role="controlgroup">
                        <label for="tel">
                            電話番号
                        </label>
                        <div><input name="tel01" type="text" id="tel01" size="4" maxlength="4" value="<?php if ( $tel01 !="" ) { echo $tel01; } ?>" pattern="[0-9]*" style="width:80px; float:left">
                        <input name="tel02" type="text" id="tel02" size="4" maxlength="4" value="<?php if ( $tel02 !="" ) { echo $tel02; } ?>" pattern="[0-9]*" style="width:80px; float:left">
                        <input name="tel03" type="text" id="tel03" size="4" maxlength="4" value="<?php if ( $tel03 !="" ) { echo $tel03; } ?>" pattern="[0-9]*" style="width:80px; float:left"></div>
                    </fieldset>
                </div>
                <div data-role="fieldcontain">
                    <fieldset data-role="controlgroup">
                        <label for="mail">
                            メールアドレス
                        </label>
                        <input name="mail" type="text" id="mail" size="34" value="<?php if ( $mail !="" ) { echo $mail; } ?>">
                    </fieldset>
                </div>
                <div data-role="fieldcontain">
                    <fieldset data-role="controlgroup">
                        <label for="comment">
                            質問・要望など
                        </label>
                        <textarea name="comment" id="comment">
<?php
if ( $comment !="" ) {
	$comment = str_replace("<br />","\n",$comment);
	echo $comment;
}
?>
                        </textarea>
                    </fieldset>
                </div>



<div style="text-align:center"><input type="submit" name="Submit" data-theme="a" value="確認" data-inline="true" /></div>
</form>



<script language="JavaScript" type="text/JavaScript">
<!--
function addBookmark(title,url) {
if (window.sidebar) {
window.sidebar.addPanel(title, url,"");
} else if( document.all ) {
window.external.AddFavorite( url, title);
} else if( window.opera && window.print ) {
return true;
}
}
//-->
</script>
</body>
</html>
