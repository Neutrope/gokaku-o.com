<?
function JcodeConvert($str, $from, $to){
//0:AUTO DETECT
//1:EUC-JP
//2:Shift_JIS
//3:ISO-2022-JP(JIS)
if($from == 0) $from = AutoDetect($str);
if($from == 1 && $to == 2) return EUCtoSJIS($str);
if($from == 1 && $to == 3) return EUCtoJIS($str);
if($from == 2 && $to == 1) return SJIStoEUC($str);
if($from == 2 && $to == 3) return SJIStoJIS($str);
if($from == 3 && $to == 1) return JIStoEUC($str);
if($from == 3 && $to == 2) return JIStoSJIS($str);
return $str;
}

function AutoDetect($str){
$b = unpack("C*", $str);
$n = count($b);
$is_ascii = TRUE;
for($i = 1; $i < $n; $i++){
if($b[$i] == 0x1b && $b[$i+1] == 0x24) return 3; //JIS
if($b[$i] <= 0x7F) continue; //ASCII
$is_ascii = FALSE;
if($b[$i] <= 0x9F) return 2; //SJIS
if($b[$i] <= 0xDF) return 1; //EUC
if($b[$i] >= 0xF0) return 2; //SJIS
if($b[$i+1] <= 0xA0) return 2; //SJIS
if($b[$i+1] >= 0xFD) return 1; //EUC
$i++;
}
if($is_ascii) return 0;
return 5;
}

function HANtoZEN($str, $encode){
//0:AUTO DETECT
//1:EUC-JP
//2:Shift_JIS
//3:ISO-2022-JP(JIS)
if($encode == 0) $encode = AutoDetect($str);
if($encode == 1) return HANtoZEN_EUC($str);
if($encode == 2) return HANtoZEN_SJIS($str);
if($encode == 3) return HANtoZEN_JIS($str);
return $str;
}

function JIStoSJIS($str_JIS){
$str_SJIS = '';
$mode = 0;
$b = unpack("C*", $str_JIS);
$n = count($b);
for($i = 1; $i <= $n; $i++){
if($b[$i] == 0x1B){
if(($b[$i+1] == 0x24 && $b[$i+2] == 0x42) || ($b[$i+1] == 0x24 && $b[$i+2] == 0x40)){
$mode = 1;
}elseif($b[$i+1] == 0x28 && $b[$i+2] == 0x49){
$mode = 2;
}else{
$mode = 0;
}
$i += 3;
}
if($mode == 1){
if($b[$i] & 0x01){
$b[$i] >>= 1;
if($b[$i] < 0x2F) $b[$i] += 0x71; else $b[$i] -= 0x4F;
if($b[$i+1] > 0x5F) $b[$i+1] += 0x20; else $b[$i+1] += 0x1F;
}else{
$b[$i] >>= 1;
if($b[$i] < 0x2F) $b[$i] += 0x70; else $b[$i] -= 0x50;
$b[$i+1] += 0x7E;
}
$str_SJIS .= pack("CC", $b[$i], $b[$i+1]);
$i++;
}elseif($mode == 2){
$b[$i] += 0x80;
$str_SJIS .= pack("C", $b[$i]);
}elseif($i < strlen($str_JIS)){
$str_SJIS .= pack("C", $b[$i]);
}
}
return $str_SJIS;
}

function JIStoEUC($str_JIS){
$str_EUC = '';
$mode = 0;
$b = unpack("C*", $str_JIS);
$n = count($b);
for($i = 1; $i <= $n; $i++){
if($b[$i] == 0x1B){
if(($b[$i+1] == 0x24 && $b[$i+2] == 0x42) || ($b[$i+1] == 0x24 && $b[$i+2] == 0x40)){
$mode = 1;
}elseif(($b[$i+1] == 0x28 && $b[$i+2] == 0x49)){
$mode = 2;
}else{
$mode = 0;
}
$i += 3;
}
if($mode == 1){
$b[$i] += 0x80; $b[$i+1] += 0x80;
$str_EUC .= pack("CC", $b[$i], $b[$i+1]);
$i++;
}elseif($mode == 2){
$b[$i] += 0x80;
$str_EUC .= pack("CC", 0x8E, $b[$i]);
}elseif($i < strlen($str_JIS)){
$str_EUC .= pack("C", $b[$i]);
}
}
return $str_EUC;
}

function SJIStoJIS($str_SJIS){
$str_JIS = '';
$mode = 0;
$b = unpack("C*", $str_SJIS);
$n = count($b);
for($i = 1; $i <= $n; $i++){
if(0xA1 <= $b[$i] && $b[$i] <= 0xDF){
if($mode != 2){
$mode = 2;
$str_JIS .= pack("CCC", 0x1B, 0x28, 0x49);
}
$b[$i] -= 0x80;
$str_JIS .= pack("C", $b[$i]);
}elseif($b[$i] >= 0x80){
if($mode != 1){
$mode = 1;
$str_JIS .= pack("CCC", 0x1B, 0x24, 0x42);
}
$b[$i] <<= 1;
if($b[$i+1] < 0x9F){
if($b[$i] < 0x13F) $b[$i] -= 0xE1; else $b[$i] -= 0x61;
if($b[$i+1] > 0x7E) $b[$i+1] -= 0x20; else $b[$i+1] -= 0x1F;
}else{
if($b[$i] < 0x13F) $b[$i] -= 0xE0; else $b[$i] -= 0x60;
$b[$i+1] -= 0x7E;
}
$b[$i] = $b[$i] & 0xff;
$str_JIS .= pack("CC", $b[$i], $b[$i+1]);
$i++;
}else{
if($mode != 0){
$mode = 0;
$str_JIS .= pack("CCC", 0x1B, 0x28, 0x42);
}
$str_JIS .= pack("C", $b[$i]);
}
}
if($mode != 0) $str_JIS .= pack("CCC", 0x1b, 0x28, 0x42);
return $str_JIS;
}

function SJIStoEUC($str_SJIS){
$str_EUC = '';
$b = unpack("C*", $str_SJIS);
$n = count($b);
for($i = 1; $i <= $n; $i++){
if(0xA1 <= $b[$i] && $b[$i] <= 0xDF){
$str_EUC .= pack("CC", 0x8E, $b[$i]);
}elseif($b[$i] >= 0x81){
$b[$i] <<= 1;
if($b[$i+1] < 0x9F){
if($b[$i] < 0x13F) $b[$i] -= 0x61; else $b[$i] -= 0xE1;
if($b[$i+1] > 0x7E) $b[$i+1] += 0x60; else $b[$i+1] += 0x61;
}else{
if($b[$i] < 0x13F) $b[$i] -= 0x60; else $b[$i] -= 0xE0;
$b[$i+1] += 0x02;
}
$str_EUC .= pack("CC", $b[$i], $b[$i+1]);
$i++;
}else{
$str_EUC .= pack("C", $b[$i]);
}
}
return $str_EUC;
}

function EUCtoJIS($str_EUC){
$str_JIS = '';
$mode = 0;
$b = unpack("C*", $str_EUC);
$n = count($b);
for($i = 1; $i <= $n; $i++){
if($b[$i] == 0x8E){
if($mode != 2){
$mode = 2;
$str_JIS .= pack("CCC", 0x1B, 0x28, 0x49);
}
$b[$i+1] -= 0x80;
$str_JIS .= pack("C", $b[$i+1]);
$i++;
}elseif($b[$i] > 0x8E){
if($mode != 1){
$mode = 1;
$str_JIS .= pack("CCC", 0x1B, 0x24, 0x42);
}
$b[$i] -= 0x80; $b[$i+1] -= 0x80;
$str_JIS .= pack("CC", $b[$i], $b[$i+1]);
$i++;
}else{
if($mode != 0){
$mode = 0;
$str_JIS .= pack("CCC", 0x1b, 0x28, 0x42);
}
$str_JIS .= pack("C", $b[$i]);
}
}
if($mode != 0) $str_JIS .= pack("CCC", 0x1b, 0x28, 0x42);
return $str_JIS;
}

function EUCtoSJIS($str_EUC){
$str_SJIS = '';
$b = unpack("C*", $str_EUC);
$n = count($b);
for($i = 1; $i <= $n; $i++){
if($b[$i] == 0x8E){
$str_SJIS .= pack("C", $b[$i+1]);
$i++;
}elseif($b[$i] >= 0x80){
if($b[$i] & 0x01){
$b[$i] >>= 1;
if($b[$i] < 0x6F) $b[$i] += 0x31; else $b[$i] += 0x71;
if($b[$i+1] > 0xDF) $b[$i+1] -= 0x60; else $b[$i+1] -= 0x61;
}else{
$b[$i] >>= 1;
if($b[$i] < 0x6F) $b[$i] += 0x30; else $b[$i] += 0x70;
$b[$i+1] -= 0x02;
}
$str_SJIS .= pack("CC", $b[$i], $b[$i+1]);
$i++;
}else{
$str_SJIS .= pack("C", $b[$i]);
}
}
return $str_SJIS;
}

function HANtoZEN_EUC($str_HAN){
$table_han2zen_euc = array(0xA1A3,0xA1D6,0xA1D7,0xA1A2,0xA1A6,0xA5F2,
0xA5A1,0xA5A3,0xA5A5,0xA5A7,0xA5A9,0xA5E3,0xA5E5,0xA5E7,0xA5C3,0xA1BC,
0xA5A2,0xA5A4,0xA5A6,0xA5A8,0xA5AA,0xA5AB,0xA5AD,0xA5AF,0xA5B1,0xA5B3,
0xA5B5,0xA5B7,0xA5B9,0xA5BB,0xA5BD,0xA5BF,0xA5C1,0xA5C4,0xA5C6,0xA5C8,
0xA5CA,0xA5CB,0xA5CC,0xA5CD,0xA5CE,0xA5CF,0xA5D2,0xA5D5,0xA5D8,0xA5DB,
0xA5DE,0xA5DF,0xA5E0,0xA5E1,0xA5E2,0xA5E4,0xA5E6,0xA5E8,0xA5E9,0xA5EA,
0xA5EB,0xA5EC,0xA5ED,0xA5EF,0xA5F3,0xA1AB,0xA1AC);
$str_ZEN = '';
$b = unpack("C*", $str_HAN);
$n = count($b);
for($i = 1; $i <= $n; $i++){
if($b[$i] == 0x8E){
$b[$i+1] -= 0xA1;
$c1 = (($table_han2zen_euc[$b[$i+1]] & 0xff00) >> 8);
$c2 = ($table_han2zen_euc[$b[$i+1]] & 0x00ff);
$str_ZEN .= pack("CC", $c1, $c2);
$i++;
}elseif($b[$i] >= 0xA1){
$str_ZEN .= pack("CC", $b[$i], $b[$i+1]);
$i++;
}else{
$str_ZEN .= pack("C", $b[$i]);
}
}
return $str_ZEN;
}

function HANtoZEN_SJIS($str_HAN){
$table_han2zen_sjis = array(0x8142,0x8175,0x8176,0x8141,0x8145,0x8392,
0x8340,0x8342,0x8344,0x8346,0x8348,0x8383,0x8385,0x8387,0x8362,0x815B,
0x8341,0x8343,0x8345,0x8347,0x8349,0x834A,0x834C,0x834E,0x8350,0x8352,
0x8354,0x8356,0x8358,0x835A,0x835C,0x835E,0x8360,0x8363,0x8365,0x8367,
0x8369,0x836A,0x836B,0x836C,0x836D,0x836E,0x8371,0x8374,0x8377,0x837A,
0x837D,0x837E,0x8380,0x8381,0x8382,0x8384,0x8386,0x8388,0x8389,0x838A,
0x838B,0x838C,0x838D,0x838F,0x8393,0x814A,0x814B);
$str_ZEN = '';
$b = unpack("C*", $str_HAN);
$n = count($b);
for($i = 1; $i <= $n; $i++){
if(0xA1 <= $b[$i] && $b[$i] <= 0xDF){
$b[$i] -= 0xA1;
$c1 = ($table_han2zen_sjis[$b[$i]] & 0xff00) >> 8;
$c2 = $table_han2zen_sjis[$b[$i]] & 0x00ff;
$str_ZEN .= pack("CC", $c1, $c2);
}elseif($b[$i] >= 0x80){
$str_ZEN .= pack("CC", $b[$i], $b[$i+1]);
$i++;
}else{
$str_ZEN .= pack("C", $b[$i]);
}
}
return $str_ZEN;
}

function HANtoZEN_JIS($str_HAN){
$table_han2zen_jis = array(0x2123,0x2156,0x2157,0x2122,0x2126,0x2572,
0x2521,0x2523,0x2525,0x2527,0x2529,0x2563,0x2565,0x2567,0x2543,0x213C,
0x2522,0x2524,0x2526,0x2528,0x252A,0x252B,0x252D,0x252F,0x2531,0x2533,
0x2535,0x2537,0x2539,0x253B,0x253D,0x253F,0x2541,0x2544,0x2546,0x2548,
0x254A,0x254B,0x254C,0x254D,0x254E,0x254F,0x2552,0x2555,0x2558,0x255B,
0x255E,0x255F,0x2560,0x2561,0x2562,0x2564,0x2566,0x2568,0x2569,0x256A,
0x256B,0x256C,0x256D,0x256F,0x2573,0x212B,0x212C);
$str_ZEN = '';
$mode = 0;
$b = unpack("C*", $str_HAN);
$n = count($b);
for($i = 1; $i <= $n; $i++){
if($b[$i] == 0x1B){
if($b[$i+1] == 0x28 && $b[$i+2] == 0x49){
$mode = 1;
$str_ZEN .= pack("CCC", 0x1B, 0x24, 0x42);
}else{
$mode = 0;
$str_ZEN .= pack("CCC", $b[$i], $b[$i+1], $b[$i+2]);
}
$i += 3;
}
if($mode == 1){
$b[$i] -= 0x21;
$c1 = ($table_han2zen_jis[$b[$i]] & 0xff00) >> 8;
$c2 = $table_han2zen_jis[$b[$i]] & 0x00ff;
$str_ZEN .= pack("CC", $c1, $c2);
}elseif($i < strlen($str_HAN)){
$str_ZEN .= pack("C", $b[$i]);
}
}
return $str_ZEN;
}
?>