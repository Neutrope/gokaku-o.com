//半角
function null_check(d_val){
	var dv = d_val;
	for(i=0; i<dv.length; i++){
		if (dv.substring(i, i+1)<"0" || dv.substring(i, i+1)>"9"){return false;}
	}
	return true;
}

//入力内容チェック
function formcheck(){
var fOBj = form;


//会員No.
if(fOBj.number.value==""){
	alert("\n会員No.をご入力ください。");
	fOBj.number.focus();return false;
}

//名前
if(fOBj.mname.value==""){
	alert("\nお名前をご入力ください。");
	fOBj.mname.focus();return false;
}

//メールアドレスチェック
function mail_check(d_val){
	var dv = d_val;
	if(dv.match("@")==null){return false;}
	if(dv.match("@")!=-1){
		if(dv.match(/^[a-zA-Z0-9_\-\.]/)==null){return false;}
	}
	return true;
}

//電話番号
if(fOBj.tel01.value==""){
	alert("\n市外局番を入力してしてください。");
	fOBj.tel01.focus();return false;
}
if(fOBj.tel01.value!=""){
	if (!null_check(fOBj.tel01.value)){
		alert("\n電話番号は半角数字で入力してください。");
		fOBj.tel01.focus();return false;
	}
}
if(fOBj.tel02.value==""){
	alert("\n市内局番を入力してしてください。");
	fOBj.tel02.focus();return false;
}
if(fOBj.tel02.value!=""){
	if (!null_check(fOBj.tel02.value)){
		alert("\n電話番号は半角数字で入力してください。");
		fOBj.tel02.focus();return false;
	}
}
if(fOBj.tel03.value==""){
	alert("\n電話番号を入力してしてください。");
	fOBj.tel03.focus();return false;
}
if(fOBj.tel03.value!=""){
	if (!null_check(fOBj.tel03.value)){
		alert("\n電話番号は半角数字で入力してください。");
		fOBj.tel03.focus();return false;
	}
}


//希望時間帯
if(fOBj.time1.value==""){
	alert("\n希望時間帯を選択してください。");
	fOBj.time1.focus();return false;
}
if(fOBj.time2.value==""){
	alert("\n希望時間帯してください。");
	fOBj.time2.focus();return false;
}

//メールアドレス
if(fOBj.mail.value==""){
		alert("\nメールアドレスを入力してください。");
		fOBj.mail.focus();return false;
}
if(fOBj.mail.value!=""){
 intIndex = fOBj.mail.value.indexOf('@', 0);
	if (!mail_check(fOBj.mail.value)){
		alert("メールアドレスに誤りがあります。\nもう一度入力して下さい。");
		fOBj.mail.focus();return false;}
	if (intIndex == -1){
		alert("メールアドレスに誤りがあります。\nもう一度入力して下さい。");
		fOBj.mail.focus();return false;}
	if (intIndex == 0){
		alert("メールアドレスに誤りがあります。\nもう一度入力して下さい。");
		fOBj.mail.focus();return false;}
	if (intIndex == (fOBj.mail.value.length - 1)){
		alert("メールアドレスに誤りがあります。\nもう一度入力して下さい。");
		fOBj.mail.focus();return false;}
 var check = /.+@.+\..+/;
	if (!fOBj.mail.value.match(check)){
		alert("メールアドレスに誤りがあります。\nもう一度入力して下さい。");
		fOBj.mail.focus();return false;}
}


//問合せ内容
if(fOBj.target1.checked==false&&fOBj.target2.checked==false&&fOBj.target3.checked==false&&fOBj.target4.checked==false&&fOBj.target5.checked==false&&fOBj.target6.checked==false){
	alert("\nお問い合わせ内容について教えて下さい。");
	fOBj.target1.focus();return false;}



return true;
}

