//------------------------------------------------------------------------------
//スタイルシート振分け
//------------------------------------------------------------------------------
var ns4,ns6,ie;
var Mac = navigator.appVersion.indexOf('Mac',0) != -1;
var Win = navigator.appVersion.indexOf('Win',0) != -1;
bname = navigator.appName;
ver = navigator.appVersion;
int_ver = parseInt(ver);
if (bname.indexOf("Netscape") >= 0 && int_ver < 5) ns4 = 1;
if (bname.indexOf("Netscape") >= 0 && int_ver >= 5) ns6 = 1;
if (bname.indexOf("Microsoft Internet Explorer") >=0 && int_ver >= 4) ie = 1;
if (ns4){
	if(Mac){var csstype='mac_ns4';}
	else if(Win){var csstype='win_ns4';}}
if (ns6){
	if(Mac){var csstype='mac_ns6';}
	else if(Win){var csstype='win_ns6';}}
if (ie){
	if(Mac){var csstype='mac_ie';}
	else if(Win){var csstype='win_ie';}}
if (csstype)
{	document.write('<link rel=stylesheet type="text/css" ')
	document.write('href=/library/css/')
	document.write(csstype + '.css>')}

//------------------------------------------------------------------------------
//子画面表示
//------------------------------------------------------------------------------
function subwin500(url) {
	var SW;
	SW= window.open(url,"WFOCUS","width=500,height=375,location=0,scrollbars=no,resizable=no,menubar=0,status=0");
	SW.focus();
}

//------------------------------------------------------------------------------
//NN4用リサイズ対策
//------------------------------------------------------------------------------
/*
if(!window.saveInnerWidth) {
  window.onresize = resizeIt;
  window.saveInnerWidth = window.innerWidth;
  window.saveInnerHeight = window.innerHeight;}
function resizeIt() {
    if (saveInnerWidth < window.innerWidth || 
        saveInnerWidth > window.innerWidth || 
        saveInnerHeight > window.innerHeight || 
        saveInnerHeight < window.innerHeight ) 
    {window.history.go(0);}}
*/

//------------------------------------------------------------------------------
//画像処理
//------------------------------------------------------------------------------
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}



