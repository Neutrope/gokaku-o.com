//半角
function null_check(d_val){
	var dv = d_val;
	for(i=0; i<dv.length; i++){
		if (dv.substring(i, i+1)<"0" || dv.substring(i, i+1)>"9"){return false;}
	}
	return true;
}
//郵便番号チェック
function zip_check(d_val){
	var dv = d_val;
	if(dv.match(/^[0-9]/)==null){return false;}
	return true;
}
//メールアドレスチェック
function mail_check(d_val){
	var dv = d_val;
	if(dv.match("@")==null){return false;}
	if(dv.match("@")!=-1){
		if(dv.match(/^[a-zA-Z0-9_\-\.]/)==null){return false;}
	}
	return true;
}
//入力内容チェック
function formcheck(){
var fOBj = form;

//タイトル
var chkbox = fOBj["kind[]"];
var a02flag=false;
for(i=0;i<chkbox.length;i++){
if(chkbox[i].checked==true)
a02flag=true;
}
if(!a02flag) {
window.alert("お問い合わせ内容をどれか1つ以上選択して下さい。");
return false;}

//名前
if(fOBj.name01.value==""){
	alert("\nご本人様の名前をご入力ください。");
	fOBj.name01.focus();return false;
}
if(fOBj.name02.value==""){
	alert("\nご本人様のふりがなをご入力ください。");
	fOBj.name02.focus();return false;
}
if(fOBj.name03.value==""){
	alert("\n保護者の方の名前をご入力ください。");
	fOBj.name03.focus();return false;
}
if(fOBj.name04.value==""){
	alert("\n保護者の方のふりがなをご入力ください。");
	fOBj.name04.focus();return false;
}

//学年
if(fOBj.school.value==""){
	alert("\n学年を選択してください。");
	fOBj.school.focus();return false;
}

//性別
if(fOBj.sex.value==""){
	alert("\n性別を選択してください。");
	fOBj.sex.focus();return false;
}

//住所
if(fOBj.zip01.value==""){
	alert("\nご住所：郵便番号をご入力ください。");
	fOBj.zip01.focus();return false;
}
if(fOBj.zip01.value!=""){
	if (!zip_check(fOBj.zip01.value)){
		alert("\n郵便番号は半角英数字で入力してください。");
		fOBj.zip01.focus();return false;
	}
}
if(fOBj.zip02.value==""){
	alert("\nご住所：郵便番号をご入力ください。");
	fOBj.zip02.focus();return false;
}
if(fOBj.zip02.value!=""){
	if (!zip_check(fOBj.zip02.value)){
		alert("\n郵便番号は半角英数字で入力してください。");
		fOBj.zip02.focus();return false;
	}
}
if(fOBj.add01.value==""){
	alert("\nご住所：都道府県を選択してください。");
	fOBj.add01.focus();return false;
}
if(fOBj.add02.value==""){
	alert("\nご住所を入力してください。");
	fOBj.add02.focus();return false;
}

//電話番号
if(fOBj.tel01.value==""){
	alert("\n市外局番を入力してしてください。");
	fOBj.tel01.focus();return false;
}
if(fOBj.tel01.value!=""){
	if (!null_check(fOBj.tel01.value)){
		alert("\n電話番号は半角数字で入力してください。");
		fOBj.tel01.focus();return false;
	}
}
if(fOBj.tel02.value==""){
	alert("\n市内局番を入力してしてください。");
	fOBj.tel02.focus();return false;
}
if(fOBj.tel02.value!=""){
	if (!null_check(fOBj.tel02.value)){
		alert("\n電話番号は半角数字で入力してください。");
		fOBj.tel02.focus();return false;
	}
}
if(fOBj.tel03.value==""){
	alert("\n電話番号を入力してしてください。");
	fOBj.tel03.focus();return false;
}
if(fOBj.tel03.value!=""){
	if (!null_check(fOBj.tel03.value)){
		alert("\n電話番号は半角数字で入力してください。");
		fOBj.tel03.focus();return false;
	}
}

//メールアドレス
if(fOBj.mail.value==""){
		alert("\nメールアドレスを入力してください。");
		fOBj.mail.focus();return false;
}
if(fOBj.mail.value!=""){
 intIndex = fOBj.mail.value.indexOf('@', 0);
	if (!mail_check(fOBj.mail.value)){
		alert("メールアドレスに誤りがあります。\nもう一度入力して下さい。");
		fOBj.mail.focus();return false;}
	if (intIndex == -1){
		alert("メールアドレスに誤りがあります。\nもう一度入力して下さい。");
		fOBj.mail.focus();return false;}
	if (intIndex == 0){
		alert("メールアドレスに誤りがあります。\nもう一度入力して下さい。");
		fOBj.mail.focus();return false;}
	if (intIndex == (fOBj.mail.value.length - 1)){
		alert("メールアドレスに誤りがあります。\nもう一度入力して下さい。");
		fOBj.mail.focus();return false;}
 var check = /.+@.+\..+/;
	if (!fOBj.mail.value.match(check)){
		alert("メールアドレスに誤りがあります。\nもう一度入力して下さい。");
		fOBj.mail.focus();return false;}
}



return true;
}

