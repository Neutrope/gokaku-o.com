// 右クリック禁止
window.onload = function() {
	document.oncontextmenu = function(){
		return false;
	}
}

$(document).bind("mobileinit", function(){
  $.mobile.ajaxEnabled = false;
  $.mobile.ajaxLinksEnabled = false;
  $.mobile.ajaxFormsEnabled = false;
  $.mobile.hashListeningEnabled = false;
});