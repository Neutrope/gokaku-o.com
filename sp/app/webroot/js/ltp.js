    window.onload = function() {
        document.oncontextmenu = function(){
            return false;
        }
    }

    $( document ).ready( function()
    {
        var interval = 500;

        $( "a" ).bind( "touchstart", function()
        {
            timer = setTimeout( function()
            {
                alert( "動画の保存はできません" );
            }, interval );

            function clearFunction()
            {
                clearTimeout( timer );
            }

            $( "a" ).bind( "touchend touchmove touchcancel", clearFunction );
        });
    });
