$(function(){
	//when onload
	jQuery.event.add(window, "load", myInit);
		//when window landscape
    var timer = false;
    $(window).resize(function() {
		if (timer !== false) {
			clearTimeout(timer);
		}
		timer = setTimeout(myInit, 200);
    });
		//init
	var myInit = function(){
		$('#b').each(function(){
			$(this).width($(window).width());
		});
	}
})
