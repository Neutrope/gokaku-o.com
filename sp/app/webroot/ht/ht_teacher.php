<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="Shift_JIS">
<meta name="viewport" content="width=device-width, user-scalable=no">
<title>家庭教師のご紹介｜家庭教師＆個別指導の合格王</title>
<link href="../../css/base3.css" rel="stylesheet" type="text/css" media="screen,print">
		<meta name="description" content="首都圏13,000人の家庭教師のなかからベストな講師をご紹介します。一部家庭教師を顔写真付きでご紹介しています。">
		<meta name="Keywords" content="家庭教師, インターネット家庭教師, 顔写真, 写真, 家庭教師一覧">
<link href="../../css/system.css" rel="stylesheet" type="text/css" media="screen,print">
<link href="../css/import.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/config.js"></script>
</head>
<body>

<table width="100%">
<tr>
<td>
<div style="background-color:#333"><span style="color:#FFF">お問い合わせ・資料請求</span></div>
</td>
</tr>
<tr>
<td>
<p>合格王は「期待通り」ではなく、「期待以上」の講師を必ず紹介！<br />合格王ならではのWebダイレクト指名！<br />登録家庭教師 約13,000人の中から一部を公開！<br />公開している家庭教師の他にもたくさん在籍していますので、お気軽にお問い合わせください。</p>
</td>
</tr>
</table>

<!--■ここからがコンテンツ■-->
    <?php
$logfile = "../../library/include/teacher_list.csv";
$fname = file($logfile);
$date = date( "Y年m月j日", filemtime($logfile) );
?>
<h3 class="bar_theacher">登録家庭教師一部抜粋　<? echo $date; ?>現在</h3>
<TABLE class="table1">
<TR>
<TD>
<TABLE width="580" border="0" cellpadding="0" cellspacing="0" class="font_Bold">
<TR>
<TD width="52"><br></TD>
<TD width="92" class="txt12n">名前</TD>
<TD width="110" class="txt12n">在籍大学</TD>
<TD width="150" class="txt12n">出身高校</TD>
<TD width="180" class="txt12n">ポイント</TD>
<TD width="30">&nbsp;</TD>
</TR>
</TABLE>

<TR><TD>
<?php
//記事の決定
$count = count($fname);
for($i = 0; $i < $count; $i++){
	list($no,$name, $univ, $div,$grade,$high,$topics,$fav,$comment)=explode(",", $fname[$i]);
	
	if ( $no != "" ) {

		//NOの２桁表示
		if ( $no < 10 ) { $uid = "00" . $no; } 
		elseif ( $no > 9 && $no <100 ) { $uid = "0" . $no; }
		else { $uid = $no; }	

		//画像の特定
		$face = "../../library/images/ht/ht_teacher_face" . $uid . ".jpg";
		if (!file_exists ( $face ) ) { $face = "../../library/images/ht/ht_teacher_face000.jpg"; }

		echo "<TABLE width=\"580\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
		echo "<TR>\n";
		echo "<TD>\n";
		echo "<TABLE border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n";
		echo "<TR>\n";
		echo "<TD width=\"50\">";
		echo "<A href=\"ht_teacher02.php?no=" . $no . "\"><IMG src=\"" . $face . "\" width=\"40\" height=\"40\" border=\"0\" alt=\"" . $name . "\"></A></TD>\n";
		echo "<TD width=\"91\" class=\"txt12n\"><A href=\"ht_teacher02.php?no=" . $no . "\">" . $name . "</A></TD>\n";
		echo "<TD width=\"110\" class=\"txt10n\">" . $univ . "</TD>\n";
		echo "<TD width=\"150\" class=\"txt10n\">" . $high . "</TD>\n";
		echo "<TD width=\"180\" class=\"txt10n\">" . $topics . "</TD>\n";
		echo "<TD width=\"30\" class=\"txt10n\"><A href=\"ht_teacher02.php?no=" . $no . "\">詳細</A></TD>\n";
		echo "</TR>\n";
		echo "</TABLE>\n";
		echo "</TD>\n";
		echo "</TR>\n";
		echo "</TABLE>\n";
	}
}
?><br>
		</TD>
</TR>
</TABLE>
</div>



<script language="JavaScript" type="text/JavaScript">
<!--
function addBookmark(title,url) {
if (window.sidebar) {
window.sidebar.addPanel(title, url,"");
} else if( document.all ) {
window.external.AddFavorite( url, title);
} else if( window.opera && window.print ) {
return true;
}
}
//-->
</script>

<script type="text/javascript"><!--
document.write("<img src='http://www.gokaku-o.com/cgi/acc/acclog.cgi?");
document.write("referrer="+document.referrer+"&");
document.write("width="+screen.width+"&");
document.write("height="+screen.height+"&");
document.write("color="+screen.colorDepth+"'>");
// -->
</script>
</body>
</html>
