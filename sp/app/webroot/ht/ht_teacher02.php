<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, user-scalable=no">
<title>家庭教師のご紹介｜家庭教師＆個別指導の合格王</title>
<SCRIPT language="JavaScript1.2" src="/library/js/base.js"></SCRIPT>
		<meta name="description" content="首都圏13,000人の家庭教師のなかからベストな講師をご紹介します。一部家庭教師を顔写真付きでご紹介しています。">
		<meta name="Keywords" content="家庭教師, インターネット家庭教師, 顔写真, 写真, 家庭教師一覧">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/config.js"></script>
<script type="text/javascript" src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="../js/jquery.inc-6.js"></script>
</head>
<body id="b">
<div data-role="header" data-position="inline" data-theme="a"><a rel="external" data-ajax="false" href="javascript:history.back();" data-theme="a" data-corners="false">back</a><h1></h1></div>

<div data-role="header" data-theme="a"><p>講師の紹介</p></div>

<p>合格王は「期待通り」ではなく、「期待以上」の講師を必ず紹介！<br />合格王ならではのWebダイレクト指名！<br /><br />登録家庭教師 約13,000人の中から一部を公開！<br />公開している家庭教師の他にもたくさん在籍していますので、お気軽にお問い合わせください。</p>

<!--■ここからがコンテンツ■-->
    <TABLE class="table1">
      <TR>
        <TD><?php
$logfile = "../../library/include/teacher_list.csv";
$fname = file($logfile);
$date = date( "Y年m月j日", filemtime($logfile) );

//記事の決定
$count = count($fname);
for($i = 0; $i < $count; $i++){
	list($n_no,$name, $univ, $div,$grade,$high,$topics,$fav,$comment)=explode(",", $fname[$i]);
	
	//NOの２桁表示
	if ( $n_no < 10 ) { $uid = "00" . $n_no; } 
	elseif ( $n_no > 9 && $n_no <100 ) { $uid = "0" . $n_no; }
	else { $uid = $n_no; }

	if ( $no == $uid ) {
		//画像の特定
		$face = "../../library/images/ht/ht_teacher_face" . $uid . ".jpg";
		if (!file_exists ( $face ) ) { $face = "../../library/images/ht/ht_teacher_face000.jpg"; }

		$comment = str_replace("\n","",$comment);
		$comment = str_replace("\r","",$comment);
		if ( $comment == "" ) { $comment = "<I>準備中</I>"; }
		
		echo "<TABLE width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
		echo "<TR><A name=\"" . $no . "\"></A>\n";
		echo "<TD id=\"tsky\">" . $name . "</TD>\n";
		echo "</TR>\n";
		echo "<TR>\n";
		echo "</TR>\n";
		echo "</TABLE>\n";
		echo "<TABLE width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
		echo "<TR valign=\"top\">\n";
		echo "<TD width=\"110\"><IMG src=\"" . $face . "\" width=\"100\" height=\"100\"></TD>\n";
		echo "<TD width=\"470\">\n";
		echo "<TABLE width=\"460\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">\n";
		echo "<TR>\n";
		?>
        <TD class="font_Bold">家庭教師のプロフィール</TD>
        <?php
		echo "</TR>\n";
		echo "<TR>\n";
		echo "<TD>在籍大学：" . $univ . $div . $grade . "<BR>\n";
		echo "出身高校：" . $high . "<BR>\n";
		//	if ( $junior != "" ) { echo "中学受験：" . $junior . "合格<BR>\n"; }
		echo "ポイント：" . $topics . "</TD>\n";
		echo "</TR>\n";
		echo "<TR>\n";
		?>
        <TD class="font_Bold"><br>
          ひとこと</TD>
        <?php
        echo "</TR>\n";
		echo "<TR>\n";
		echo "<TD>" . $comment . "</TD>\n";
		echo "</TR>\n";
		echo "<TR>\n";
		echo "<FORM action=\"/service/index.php\" method=\"POST\">\n";?>
        <TD><br>
        <INPUT type="image" src="../../images/gn-top-button_m.gif" alt="この講師について問い合わせてみる" name="Image25" width="249" height="35" class="float_Rimg" onMouseOver="this.src='../../images/gn-top-button_o.gif';" onMouseOut="this.src='../../images/gn-top-button_m.gif';"></TD>
        <?php
		echo "<INPUT type=\"hidden\" name=\"t\" value=\"1\">\n";
		echo "<INPUT type=\"hidden\" name=\"k\" value=\"" . $name . "\">\n";
		echo "</TD>\n";
		echo "</FORM>\n";
		echo "</TR>\n";
		echo "</TABLE>\n";
		echo "</TD>\n";
		echo "</TR>\n";
		echo "</TABLE>\n";
	}
}
?>
        </TD>
      </TR>
  </TABLE>

<script language="JavaScript" type="text/JavaScript">
<!--
function addBookmark(title,url) {
if (window.sidebar) {
window.sidebar.addPanel(title, url,"");
} else if( document.all ) {
window.external.AddFavorite( url, title);
} else if( window.opera && window.print ) {
return true;
}
}
//-->
</script>

<script type="text/javascript"><!--
document.write("<img src='http://www.gokaku-o.com/cgi/acc/acclog.cgi?");
document.write("referrer="+document.referrer+"&");
document.write("width="+screen.width+"&");
document.write("height="+screen.height+"&");
document.write("color="+screen.colorDepth+"'>");
// -->
</script>
</body>
</html>
