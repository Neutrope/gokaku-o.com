<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, user-scalable=no">
<title>会員専用ページ｜家庭教師＆個別指導の合格王</title>
<SCRIPT language="JavaScript1.2" src="../library/js/base.js"></SCRIPT>
<SCRIPT language="JavaScript1.2" src="../library/js/check_contact.js"></SCRIPT>
		<meta name="description" content="家庭教師なら家庭教師の合格王。家庭教師についてのお問い合せ・資料請求はこちらのフォームからどうぞ。">
		<meta name="Keywords" content="家庭教師, プロ家庭教師, 中学受験, インターネット家庭教師, お問い合せ, 資料, 資料請求, 質問">
<link href="../css/import.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/config.js"></script>
<script type="text/javascript" src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="../js/jquery.inc-6.js"></script>
</head>
<body id="b">
<div data-role="header" data-position="inline" data-theme="a"><a rel="external" data-ajax="false" href="javascript:history.back();" data-theme="a" data-corners="false">back</a><h1>会員専用ページ</h1></div>

<div data-role="header" data-theme="a"><p>ご相談・ご要望</p></div>

<div id="container">

<p>こちらのフォームは会員専用のページとなります。<br />
			ご新規のお客様は<a href="../contact/index.php" target="_top"><b>こちら</b></a>よりお問い合せください。<br />
			<br />
			<br />
			以下のフォームにご記入のうえ、「確認」ボタンを押してください。<br />
			※こちらの内容を担当家庭教師に直接見せることはございません。</p>
<br />
		<form method="post" action="mail.php">
                <div data-role="fieldcontain">
                    <fieldset data-role="controlgroup">
                        <label for="textinput1">
                            会員No.
                        </label>
                        <input name="会員No." id="textinput1" pattern="[0-9]*" placeholder="" value="" type="text" />
                    </fieldset>
                </div>
                <div data-role="fieldcontain">
                    <fieldset data-role="controlgroup">
                        <label for="textinput2">
                            お名前
                        </label>
                        <input name="お名前" id="textinput2" placeholder="" value="" type="text" />
                    </fieldset>
                </div>
                <div data-role="fieldcontain">
                    <fieldset data-role="controlgroup">
                        <label for="textinput3">
                            メールアドレス
                        </label>
                        <input name="メールアドレス" id="textinput3" placeholder="" value="" type="text" />
                    </fieldset>
                </div>
                <div data-role="fieldcontain">
                    <fieldset data-role="controlgroup">
                        <label for="textinput4">
                            ご希望の連絡先｜TEL
                        </label>
                        <input name="ご希望の連絡先｜TEL" id="textinput4" pattern="[0-9]*" placeholder="" value="" type="text" />
                    </fieldset>
                </div>
                <div data-role="fieldcontain">
                    <label for="selectmenu1">
                        ご希望の時間帯
                    </label>
                    <select name="ご希望の時間帯～">
										<option selected value="時間選択なし">時間選択～</option>
										<option value="PM１">PM１時から</option>
										<option value="PM２">PM２時から</option>
										<option value="PM３">PM３時から</option>
										<option value="PM４">PM４時から</option>
										<option value="PM５">PM５時から</option>
										<option value="PM６">PM６時から</option>
										<option value="PM７">PM７時から</option>
										<option value="PM８">PM８時から</option>
										<option value="PM９">PM９時から</option>
                    </select>
                </div>
                <div data-role="fieldcontain">
                    <label for="selectmenu4">
                    </label>
                    <select name="～ご希望の時間帯">
										<option selected value="時間選択なし">～時間選択</option>
										<option value="PM２">PM２時まで</option>
										<option value="PM３">PM３時まで</option>
										<option value="PM４">PM４時まで</option>
										<option value="PM５">PM５時まで</option>
										<option value="PM６">PM６時まで</option>
										<option value="PM７">PM７時まで</option>
										<option value="PM８">PM８時まで</option>
										<option value="PM９">PM９時まで</option>
										<option value="PM10">PM10時まで</option>
                    </select>
                </div>
                <div id="checkboxes1" data-role="fieldcontain">
                    <fieldset data-role="controlgroup" data-type="vertical">
                        <legend>
                            お問い合せ内容
                        </legend>
                        <input id="checkbox1" name="指導内容について" type="checkbox" />
                        <label for="checkbox1">
                            指導内容について
                        </label>
                        <input id="checkbox2" name="指導日程について" type="checkbox" />
                        <label for="checkbox2">
                            指導日程について
                        </label>
                        <input id="checkbox3" name="問題集について" type="checkbox" />
                        <label for="checkbox3">
                            問題集について
                        </label>
                        <input id="checkbox4" name="担当家庭教師について" type="checkbox" />
                        <label for="checkbox4">
                            担当家庭教師について
                        </label>
                        <input id="checkbox5" name="受験情報など" type="checkbox" />
                        <label for="checkbox5">
                            受験情報など
                        </label>
                        <input id="checkbox6" name="契約等について" type="checkbox" />
                        <label for="checkbox6">
                            契約等について
                        </label>
                    </fieldset>
                </div>
                <div data-role="fieldcontain">
                    <fieldset data-role="controlgroup">
                        <label for="textarea1">
                            ご相談・ご要望
                        </label>
                        <textarea name="ご相談・ご要望" id="textarea1" placeholder=""></textarea>
                    </fieldset>
                </div>
                <div style="text-align:center"><input type="submit" data-theme="a" value="　確 認　" data-inline="true" /><input type="reset" value="リセット" data-theme="a" data-inline="true" /></div>
		</form>

<br />

		<a rel="external" data-ajax="false" href="tel:0120041590">
		<div class="maxw"><img src="../img/banner-footer02.png" alt="0120-041-590"></div>
		</a>
<ul data-role="listview" data-theme="d">
	<li><a rel="external" data-ajax="false" href="../area.html"><h3>対応エリア</h3></a></li>
	<li><a rel="external" data-ajax="false" href="../profile.html"><h3>会社案内</h3></a></li>
	<li><a rel="external" data-ajax="false" href="../policy.html"><h3>プライバシーポリシー</h3></a></li>
</ul>
	<!--</div>--><!-- /content -->
<div style="clear: both;"></div>
<div data-role="footer" data-theme="c"><p class="center">&copy;Copyright Gokakuoh Inc. All rights reserved.</p></div>
</div><!-- /container -->
</body>
</html>