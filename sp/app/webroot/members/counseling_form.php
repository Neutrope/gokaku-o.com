<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<title>ご相談・ご要望｜家庭教師＆個別指導の合格王</TITLE>
<link href="../css/base2.css" rel="stylesheet" type="text/css" media="screen,print" />
<SCRIPT language="JavaScript1.2" src="/library/js/base.js"></SCRIPT>
<SCRIPT language="JavaScript1.2" src="/library/js/check_members.js"></SCRIPT>
<meta name="description" content="家庭教師の合格王、会員の方のご相談・ご要望はこちらのフォームよりお願いします。">
<meta name="Keywords" content="家庭教師, プロ家庭教師, 中学受験, インターネット家庭教師, 会員専用, ご相談・ご要望">
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<link href="../css/system.css" rel="stylesheet" type="text/css" media="screen,print">
</head>

<body>
<div id="container">
<div id="wrapper">


<!--■■ここからがヘッダー■■-->
<div id="header">
  <div id="button">
  <div id="ref"><a href="../contact/index.php"><br /></a></div>
  <div id="mem"><a href="index.html"><br /></a></div>
  </div>
  <h1>家庭教師なら家庭教師＆個別指導の合格王</h1>
  | <a href="../sitemap/index.html" class="sitemap">サイトマップ</a></div>


<!--■■ここからがパン屑ナビ、見出し■■-->
<div id="pan"><a href="../index.html">トップ</a> &gt; <a href="index.html">会員専用ページ</a> &gt; ご相談・ご要望</div>

<h2><img src="../images/title_counseling.gif" alt="ご相談・ご要望" width="650" height="32" /></h2>


<!--■ここからがコンテンツ■-->
<div id="main">
  <div class="contentswrap"> <img src="../images/main_price_head.gif" alt="" width="650" height="12">
    <TABLE class="table0">
      <TR>
        <TD><TABLE width="575" border="0" cellspacing="0" cellpadding="0">
          </TABLE>
            <TABLE width="575" border="0" cellspacing="0" cellpadding="0">
              <TR>
                      <TD class="txt12"><p>こちらのフォームは会員専用のページとなります。<br>
                        ご新規のお客様は<a href="../contact/index.php">こちらよりお問い合せ下さい。</a></p>
                          <br>
                          <p>以下のフォームにご記入のうえ、「確認」ボタンを押してください。<br>
                            ※こちらの内容を担当家庭教師に直接見せることはございません。</p></TD>
              </TR>
            </TABLE>
          <TABLE width="575" border="0" cellspacing="0" cellpadding="0">
              <FORM name="form" action="counseling_conf.php" method="POST" onSubmit="return formcheck();">
                <?php
if ( $tname != "" ) {
echo "<TR>\n";
echo "<TD background=\"/library/images/common/co_bar.gif\"><IMG src=\"/library/images/common/co_spacer.gif\" width=\"1\" height=\"1\"></TD>\n";
echo "</TR>\n";
echo "<TR>\n";
echo "<TD>\n";
echo "<TABLE border=\"0\" cellspacing=\"10\" cellpadding=\"0\">\n";
echo "<TR>\n";
echo "<TD width=\"100\" id=\"fbold\" class=\"txt12\">講師の選択</TD>\n";
echo "<TD class=\"txt12\">\n";
echo "</TD>\n";
echo "</TR>\n";
echo "</TABLE>\n";
echo "</TD>\n";
echo "</TR>\n";
}
?>
                <TR>
                  <TD background="/library/images/common/co_bar.gif"><IMG src="/library/images/common/co_spacer.gif" width="1" height="1"></TD>
                </TR>
                <TR>
                  <TD><TABLE border="0" cellspacing="10" cellpadding="0">
                      <TR>
                        <TD width="115" class="txt12 font_Bold">会員No.</TD>
                        <TD class="txt12"><INPUT name="number" type="text" id="number" size="34" value="<?php if ( $number !="" ) { echo $number; } ?>">                        </TD>
                      </TR>
                  </TABLE></TD>
                </TR>
                <TR>
                  <TD><TABLE border="0" cellspacing="10" cellpadding="0">
                      <TR>
                        <TD width="115" class="txt12 font_Bold">お名前</TD>
                        <TD class="txt12"><INPUT name="mname" type="text" id="mname" size="34" value="<?php if ( $mname !="" ) { echo $mname; } ?>">                        </TD>
                      </TR>
                  </TABLE></TD>
                </TR>
                <TR>
                  <TD><TABLE border="0" cellspacing="10" cellpadding="0">
                      <TR>
                        <TD width="115" class="txt12 font_Bold">メールアドレス</TD>
                        <TD class="txt12"><INPUT name="mail" type="text" id="mail" size="34" value="<?php if ( $mail !="" ) { echo $mail; } ?>">                        </TD>
                      </TR>
                  </TABLE></TD>
                </TR>
                <TR>
                  <TD background="/library/images/common/co_bar.gif"><IMG src="/library/images/common/co_spacer.gif" width="1" height="1"></TD>
                </TR>

                <TR>
                  <TD><TABLE border="0" cellspacing="10" cellpadding="0">
                      <TR>
                        <TD width="115" class="txt12 font_Bold">ご希望の連絡先</TD>
                        <TD class="txt12"><INPUT name="tel01" type="text" id="tel01" size="4" maxlength="4" value="<?php if ( $tel01 !="" ) { echo $tel01; } ?>">
                          -
                          <INPUT name="tel02" type="text" id="tel02" size="4" maxlength="4" value="<?php if ( $tel02 !="" ) { echo $tel02; } ?>">
                          -
                          <INPUT name="tel03" type="text" id="tel03" size="4" maxlength="4" value="<?php if ( $tel03 !="" ) { echo $tel03; } ?>">                        </TD>
                      </TR>
                  </TABLE></TD>
                </TR>
                <TR>
                  <TD><TABLE border="0" cellspacing="10" cellpadding="0">
                      <TR>
                        <TD width="115" class="txt12 font_Bold">ご希望の時間帯</TD>
                        <TD> <SELECT name="time1" id="time1">
                    <OPTION value="" <?php if ( $time1 =="" ) { echo "selected"; } ?>>時間選択</OPTION>
                    <OPTION value="ＰＭ１" <?php if ( $time1 =="ＰＭ１" ) { echo "selected"; } ?>>ＰＭ１</OPTION>
                    <OPTION value="ＰＭ２" <?php if ( $time1 =="ＰＭ２" ) { echo "selected"; } ?>>ＰＭ２</OPTION>
                    <OPTION value="ＰＭ３" <?php if ( $time1 =="ＰＭ３" ) { echo "selected"; } ?>>ＰＭ３</OPTION>
                    <OPTION value="ＰＭ４" <?php if ( $time1 =="ＰＭ４" ) { echo "selected"; } ?>>ＰＭ４</OPTION>
                    <OPTION value="ＰＭ５" <?php if ( $time1 =="ＰＭ５" ) { echo "selected"; } ?>>ＰＭ５</OPTION>
                    <OPTION value="ＰＭ６" <?php if ( $time1 =="ＰＭ５" ) { echo "selected"; } ?>>ＰＭ６</OPTION>
                    <OPTION value="ＰＭ７" <?php if ( $time1 =="ＰＭ６" ) { echo "selected"; } ?>>ＰＭ７</OPTION>
                    <OPTION value="ＰＭ８" <?php if ( $time1 =="ＰＭ７" ) { echo "selected"; } ?>>ＰＭ８</OPTION>
                    <OPTION value="ＰＭ９" <?php if ( $time1 =="ＰＭ８" ) { echo "selected"; } ?>>ＰＭ９</OPTION>
                    <OPTION value="ＰＭ１０" <?php if ( $time1 =="ＰＭ９" ) { echo "selected"; } ?>>ＰＭ１０</OPTION>
                  </SELECT>   
                  時〜
                  <SELECT name="time2" id="time2">
                    <OPTION value="" <?php if ( $time2 =="" ) { echo "selected"; } ?>>時間選択</OPTION>
                    <OPTION value="ＰＭ２" <?php if ( $time2 =="ＰＭ２" ) { echo "selected"; } ?>>ＰＭ２</OPTION>
                    <OPTION value="ＰＭ３" <?php if ( $time2 =="ＰＭ３" ) { echo "selected"; } ?>>ＰＭ３</OPTION>
                    <OPTION value="ＰＭ４" <?php if ( $time2 =="ＰＭ４" ) { echo "selected"; } ?>>ＰＭ４</OPTION>
                    <OPTION value="ＰＭ５" <?php if ( $time2 =="ＰＭ５" ) { echo "selected"; } ?>>ＰＭ５</OPTION>
                    <OPTION value="ＰＭ６" <?php if ( $time2 =="ＰＭ６" ) { echo "selected"; } ?>>ＰＭ６</OPTION>
                    <OPTION value="ＰＭ７" <?php if ( $time2 =="ＰＭ７" ) { echo "selected"; } ?>>ＰＭ７</OPTION>
                    <OPTION value="ＰＭ８" <?php if ( $time2 =="ＰＭ８" ) { echo "selected"; } ?>>ＰＭ８</OPTION>
                    <OPTION value="ＰＭ９" <?php if ( $time2 =="ＰＭ９" ) { echo "selected"; } ?>>ＰＭ９</OPTION>
                    <OPTION value="ＰＭ１０" <?php if ( $time2 =="ＰＭ１０" ) { echo "selected"; } ?>>ＰＭ１０</OPTION>
                  </SELECT>   
                  時                        </TD>
                      </TR>
                  </TABLE></TD>
                </TR>
                                <TR>
                  <TD background="/library/images/common/co_bar.gif"><IMG src="/library/images/common/co_spacer.gif" width="1" height="1"></TD>
                </TR>


                <TR>
                  <TD><TABLE border="0" cellspacing="10" cellpadding="0">
                      <TR>
                        <TD width="115" class="txt12 font_Bold">お問い合わせ内容</TD>
                        <TD class="txt12"><INPUT type="checkbox" name="target1" value="指導内容について" <?php if ( $target1 != "" ) { echo " checked"; } ?>>
                          指導内容について<BR>
                          <INPUT type="checkbox" name="target2" value="指導日程について" <?php if ( $target2 != "" ) { echo " checked"; } ?>>
                          指導日程について<BR>
                          <INPUT type="checkbox" name="target3" value="問題集について" <?php if ( $target3 != "" ) { echo " checked"; } ?>>
                          問題集について<BR>
                          <INPUT type="checkbox" name="target4" value="担当家庭教師について" <?php if ( $target4 != "" ) { echo " checked"; } ?>>
                          担当家庭教師について<BR>
                          <INPUT type="checkbox" name="target5" value="受験情報など" <?php if ( $target5 != "" ) { echo " checked"; } ?>>
                          受験情報など<BR>
                          <INPUT type="checkbox" name="target6" value="契約等について" <?php if ( $target6 != "" ) { echo " selected"; } ?>>
                          契約等について</TD>
                      </TR>
                  </TABLE></TD>
                <TR>
                  <TD background="/library/images/common/co_bar.gif"><IMG src="/library/images/common/co_spacer.gif" width="1" height="1"></TD>
                </TR>
                <TR>
                  <TD><TABLE width="575" border="0" cellpadding="0" cellspacing="10">
                      <TR>
                        <TD width="115" class="txt12 font_Bold">ご相談・ご要望</TD>
                        <TD width="450" class="txt12"><TEXTAREA class="font_12px" name="comment" cols="65" rows="5" wrap="HARD" id="comment">
<?php
if ( $comment !="" ) {
	$comment = str_replace("<BR>","\n",$comment);
	echo $comment;
}
?></TEXTAREA></TD>
                      </TR>
                  </TABLE></TD>
                </TR>
                <TR>
                  <TD background="/library/images/common/co_bar.gif"><IMG src="/library/images/common/co_spacer.gif" width="1" height="1"></TD>
                </TR>
                <TR>
                  <INPUT type="hidden" name="kind" value="<?php if ( $kind !="" ) { echo $kind; } ?>">
                  <TD height="30" align="center" valign="bottom"><INPUT type="submit" name="Submit" value="　　　確　認　　　">                  </TD>
                </TR>
              </FORM>
          </TABLE></TD>
      </TR>
    </TABLE>
    <img src="../images/main01_price_foot.gif" alt="" width="650" height="12"></div>
</div>
  <div id="footer_mail"><a href="../contact/index.php"><br /></a></div>
</div>

<!--■ここからがサイドメニュー■-->
<div id="menu">
<a href="../index.html"><img src="../images/logo.gif" alt="家庭教師＆個別指導の合格王" width="145" height="64" /></a>
<a href="../service/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image8','','../images/sub_mitsumori_o.gif',1)"><img src="../images/sub_mitsumori_m.gif" alt="無料お見積もり" name="Image8" width="145" height="53" id="Image8" /></a> <a href="../contact/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image9','','../images/sub-lesson_o.gif',1)"><img src="../images/sub-lesson_m.gif" alt="無料体験レッスン" name="Image9" width="145" height="53" id="Image9" /></a> <a href="../ht/index.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image24','','../images/sub-kyo_o.gif',1)"><img src="../images/sub-kyo_m.gif" alt="合格王の教育" name="Image24" width="145" height="32" id="Image24" /></a> <a href="../ht/ht_point.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image11','','../images/sub-dak_o.gif',1)"><img src="../images/sub-dak_m.gif" alt="合格王だからできること" name="Image11" width="145" height="32" id="Image11" /></a> <a href="../ht/ht_system.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image12','','../images/sub-sys_o.gif',1)"><img src="../images/sub-sys_m.gif" alt="合格王の教育システム" name="Image12" width="145" height="29" id="Image12" /></a> <a href="../ht/ht_system01.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image13','','../images/sub-sys01_o.gif',1)"><img src="../images/sub-sys01_m.gif" alt="小学生コース" name="Image13" width="145" height="21" id="Image13" /></a> <a href="../ht/ht_system02.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image14','','../images/sub-sys02_o.gif',1)"><img src="../images/sub-sys02_m.gif" alt="中学生コース" name="Image14" width="145" height="21" id="Image14" /></a> <a href="../ht/ht_system03.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image15','','../images/sub-sys03_o.gif',1)"><img src="../images/sub-sys03_m.gif" alt="高校生コース" name="Image15" width="145" height="21" id="Image15" /></a> <a href="../gonet/gn_top.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image16','','../images/sub-sys04_o.gif',1)"><img src="../images/sub-sys04_m.gif" alt="特別コース" name="Image16" width="145" height="24" id="Image16" /></a> <a href="../gonet/gn_top.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image17','','../images/sub-net_o.gif',1)"><img src="../images/sub-net_m.gif" alt="インターネット家庭教師" name="Image17" width="145" height="32" id="Image17" /></a> <a href="../ht/ht_teacher.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image18','','../images/sub-kou_o.gif',1)"><img src="../images/sub-kou_m.gif" alt="講師の紹介" name="Image18" width="145" height="32" id="Image18" /></a> <a href="../ht/ht_voice.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image19','','../images/sub-tai_o.gif',1)"><img src="../images/sub-tai_m.gif" alt="体験談" name="Image19" width="145" height="32" id="Image19" /></a> <a href="../ht/ht_price.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image20','','../images/sub-pri_o.gif',1)"><img src="../images/sub-pri_m.gif" alt="料金表" name="Image20" width="145" height="32" id="Image20" /></a> <a href="../ht/ht_flow.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image21','','../images/sub-flo_o.gif',1)"><img src="../images/sub-flo_m.gif" alt="お申し込みの流れ" name="Image21" width="145" height="32" id="Image21" /></a> <a href="../ht/ht_qa.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image22','','../images/sub-faq_o.gif',1)"><img src="../images/sub-faq_m.gif" alt="よくあるご質問" name="Image22" width="145" height="32" id="Image22" /></a> <a href="../ht/ht_safety.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image23','','../images/sub-tro_o.gif',1)"><img src="../images/sub-tro_m.gif" alt="ご存知ですか？こんなトラブル" name="Image23" width="145" height="32" id="Image23" /></a>
<ul>
<li><a href="../profile/index.html">会社概要</a></li>
<li><a href="../recruit/index.html">採用情報</a></li>
<li><a href="../recruit/rec_ht.html">家庭教師募集</a></li>
<li><a href="../profile/prof_policy.html">プライバシーポリシー</a></li>
<li><a href="../sitemap/index.html">サイトマップ</a></li>
</ul>
</div>


<!--■ここからがフッター■-->
<img src="http://www.gokaku-o.com/images/footer_head.gif" alt="" width="800" height="7" /><div id="footer"><p><a href="http://www.gokaku-o.com/index.html">家庭教師＆個人指導の合格王TOP</a>｜<a href="http://www.gokaku-o.com/tokyo.html">東京対応エリア</a>｜<a href="http://www.gokaku-o.com/kanagawa.html">神奈川対応エリア</a>｜<a href="http://www.gokaku-o.com/chiba.html">千葉対応エリア</a>｜<a href="http://www.gokaku-o.com/saitama.html">埼玉対応エリア</a><br />
  <a href="http://www.gokaku-o.com/service/index.php">無料お見積相談サービス</a>｜<a href="http://www.gokaku-o.com/contact/index.php">お問い合わせ・資料請求</a>｜<a href="http://www.gokaku-o.com/ht/index.html">合格王の教育</a>｜<a href="http://www.gokaku-o.com/ht/ht_point.html">合格王だからできること</a>｜<a href="http://www.gokaku-o.com/ht/ht_system.html">合格王の教育システム</a><br />
  <a href="http://www.gokaku-o.com/gonet/gn_top.html">インターネット家庭教師</a>｜<a href="http://www.gokaku-o.com/ht/ht_teacher.php">家庭教師のご紹介</a>｜<a href="http://www.gokaku-o.com/ht/ht_letter_tyu.html">ご利用者の声</a>｜<a href="http://www.gokaku-o.com/ht/ht_voice.html">合格体験談</a>｜<a href="http://www.gokaku-o.com/ht/ht_price.html">料金表</a>｜<a href="http://www.gokaku-o.com/ht/ht_flow.html">お申し込みの流れ</a>｜<a href="http://www.gokaku-o.com/ht/ht_qa.html">よくあるご質問</a><br>
<a href="http://www.gokaku-o.com/ht/ht_safety.html">ご存じですか？こんなトラブル</a>｜<a href="http://www.gokaku-o.com/profile/index.html">会社概要</a>｜<a href="http://www.gokaku-o.com/recruit/index.html">採用情報</a>｜<a href="http://www.gokaku-o.com/recruit/rec_ht.html">家庭教師募集</a>｜<a href="http://www.gokaku-o.com/profile/prof_policy.html">プライバシーポリシー</a>｜<a href="http://www.gokaku-o.com/sitemap/index.html">サイトマップ</a></p>
<img src="http://www.gokaku-o.com/images/footer_foot.gif" alt="" width="800" height="10" /></div>


<!--■ここからがコピーライト■-->
<p class="copyrights">Copyright (C) Gokakuoh Inc. All rights reserved.</p>

</div>


</body>
</html>
