<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, user-scalable=no">
<title>家庭教師＆個別指導の合格王</title>
<link href="../../css/import.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.1.0/jquery.mobile-1.1.0.min.css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../../js/config.js"></script>
<script type="text/javascript" src="http://code.jquery.com/mobile/1.1.0/jquery.mobile-1.1.0.min.js"></script>
<!--▼▼CSS。埋め込み時　要コピペ（head部分）CSSはお好みで自由に編集してください▼▼-->
<style type="text/css">
/* CSSはお好みで */
#news_list li{
	color:#666;
	font-size:14px;
	margin:0;
	padding:10px 5px;
	margin-bottom:3px;
	border-bottom:1px dotted #ccc;
	line-height:150%;
	list-style-type:none;
}
#news_list ul{
	margin:0 0 15px;
	padding:0;
}
#news_list a{color:#36F;text-decoration:underline;}
#news_list a:hover{color:#039;text-decoration:none;}
#news_list li a{display:block;}
</style>

<!--▲▲CSS。埋め込み時　要コピペ（head部分）▲▲-->
</head>
<body id="b">
<!--<div class="maxw"><img src="img/banner02_o.png" alt="合格体験記"></div>-->
<div data-role="header" data-position="inline" data-theme="a"><a rel="external" data-ajax="false" href="javascript:history.back();" data-theme="a" data-corners="false">back</a><h1>講師の紹介</h1></div>
<div class="maxw"><img src="../../img/main01_teacher.png" alt="続々と集まる喜びの声"></div>
<div data-role="content">

<!--▼▼トップページ埋め込み時はここから以下をコピーして任意の場所に貼り付けてください（html部は編集可）▼▼-->
<div id="news_wrap">
<ul id="news_list">
<?php 
//----------------------------------------------------------------------
// 設定ファイルの読み込みとページ独自設定　※必要に応じて変更下さい(START)
//----------------------------------------------------------------------

//設定ファイルインクルード（相対パス）※設置箇所が変わる場合は変更して下さい
include_once("../offer_list/config.php");
//このページの文字コード。
//Shift-jisは「SJIS」、EUC-JPは「EUC-JP」と指定してください。デフォルトはUTF-8。
$encodingType = 'UTF-8';
//データファイル（news.dat）の相対パス ※設置箇所が変わる場合は変更して下さい
$file_path = '../offer_list/data/news.dat';
//記事詳細ページの相対パス（このファイルから見たdetail.phpのパス）
$post_path = 'detail.php';
//表示件数（ニュースの表示数）
$news_dsp_count = 100;

//----------------------------------------------------------------------
// 設定ファイルの読み込みとページ独自設定　※必要に応じて変更下さい(END)
//----------------------------------------------------------------------

if(!$copyright){echo $warningMesse; exit;}else{
//ファイルの内容を取得　表示
$lines = newsListSortUser(file($file_path),$encodingType);
foreach($lines as $key => $val){
	if($key >= $news_dsp_count) break;
	  $lines_array = explode(",",$val);
	  $upymd = ymd2format($lines_array[1],$encodingType);//日付フォーマットの適用
	  $lines_array[3] = rtrim($lines_array[3]);
	  $title = title_format($lines_array[3],$lines_array[2],$lines_array[0],$post_path,'sp');//タイトルのフォーマットの適用
	  
	  //NEWマーク表示処理　※タグ部変更可。画像でももちOK（さらに下にある「{$new_mark}」を移動すれば表示場所を変えられます）
	  if($new_mark = new_mark_func($lines_array[1],'<span style="color:red;font-size:12px;" class="new_mark"> NEW !</span>'));
		
//ブラウザ出力（HTML部は編集可）
echo <<<EOF
<!--<li><span class="news_List_Ymd">{$upymd}{$new_mark}</span><br> <span class="news_List_Title">{$title} </span></li>-->

<table width="100%" cellspacing="1">
  <tr>
    <td colspan="3" style="padding:5px; border:1px #999 solid; background-color:#FFF">最寄駅｜{$lines_array[2]}</td>
    <td width="20%" rowspan="3" align="center" style="padding:5px; border:1px #999 solid; background-color:#F0F0F0">{$upymd}{$new_mark}<br />エントリー希望を送る{$title}</td>
  </tr>
  <tr>
    <td width="20%" style="padding: 5px; border:1px #999 solid; text-align: center; background-color:#FFF">学年</td>
    <td width="12%" style="padding: 5px; border:1px #999 solid; text-align: center; background-color:#FFF">性別</td>
    <td style="padding: 5px; border:1px #999 solid; text-align: center; background-color:#FFF">希望曜日</td>
    </tr>
  <tr>
    <td colspan="3" style="padding:5px; border:1px #999 solid; background-color:#FFF">{$lines_array[3]}</td>
    </tr>
</table>

EOF;

}	
?>
</ul>
<?php echo copyright_dsp($encodingType,$copyright); }//著作権表記削除不可?>
</div>
<!--▲▲トップページ埋め込み時　コピーここまで▲▲-->

<table width="100%" cellspacing="1">
  <tr>
    <td colspan="3" style="padding:5px; border:1px #999 solid; background-color:#FFF">最寄駅</td>
    <td width="20%" rowspan="3" align="center" style="padding:5px; border:1px #999 solid; background-color:#F0F0F0">エントリー希望を送る</td>
  </tr>
  <tr>
    <td width="20%" style="padding: 5px; border:1px #999 solid; text-align: center; background-color:#FFF">学年</td>
    <td width="12%" style="padding: 5px; border:1px #999 solid; text-align: center; background-color:#FFF">性別</td>
    <td style="padding: 5px; border:1px #999 solid; text-align: center; background-color:#FFF">希望曜日</td>
    </tr>
  <tr>
    <td colspan="3" style="padding:5px; border:1px #999 solid; background-color:#FFF">ニーズ</td>
    </tr>
</table>

<table width="100%" cellspacing="1">
  <tr>
    <td colspan="3" style="padding:5px; border:1px #999 solid; background-color:#F0F0FF">最寄駅｜横浜駅（JR東海道線他）</td>
    <td width="20%" rowspan="3" align="center" style="padding:5px; border:1px #999 solid; background-color:#F0F0F0">エントリー希望を送る</td>
  </tr>
  <tr>
    <td width="20%" style="padding: 5px; border:1px #999 solid; text-align: center; background-color:#F0F0FF">中３</td>
    <td width="12%" style="padding: 5px; border:1px #999 solid; text-align: center; background-color:#F0F0FF">男</td>
    <td style="padding: 5px; border:1px #999 solid; text-align: center; background-color:#F0F0FF">週２：月<span style="color: #CCC">火</span>水<span style="color: #CCC">木</span>金<span style="color: #CCC">土日</span></td>
    </tr>
  <tr>
    <td colspan="3" style="padding:5px; border:1px #999 solid; background-color:#F0F0FF">数学の弱点を克服したい</td>
    </tr>
</table>

<table width="100%" cellspacing="1">
  <tr>
    <td colspan="3" style="padding:5px; border:1px #999 solid; background-color:#FFF0F0">最寄駅｜渋谷駅（JR山手線他）</td>
    <td width="20%" rowspan="3" align="center" style="padding:5px; border:1px #999 solid; background-color:#F0F0F0">エントリー希望を送る</td>
  </tr>
  <tr>
    <td width="20%" style="padding: 5px; border:1px #999 solid; text-align: center; background-color:#FFF0F0">小５</td>
    <td width="12%" style="padding: 5px; border:1px #999 solid; text-align: center; background-color:#FFF0F0">女</td>
    <td style="padding: 5px; border:1px #999 solid; text-align: center; background-color:#FFF0F0">週１：<span style="color: #CCC">月火水木金</span>土日</td>
    </tr>
  <tr>
    <td colspan="3" style="padding:5px; border:1px #999 solid; background-color:#FFF0F0">中学受験に向けて全教科</td>
    </tr>
</table>

</div>
		<a rel="external" data-ajax="false" href="tel:0120041590">
		<div class="maxw"><img src="../../img/banner-footer02.png" alt="0120-041-590"></div>
		</a>
<ul data-role="listview" data-theme="d">
	<li><a rel="external" data-ajax="false" href="../../area.html"><h3>対応エリア</h3></a></li>
	<!--<li><a rel="external" data-ajax="false" href="service/service_form.php"><h3>無料お見積相談サービス</h3></a></li>-->
	<li><a rel="external" data-ajax="false" href="../../contact/index.php"><h3>お問い合わせ・資料請求</h3></a></li>
	<li><a rel="external" data-ajax="false" href="../../profile.html"><h3>会社案内</h3></a></li>
	<li><a rel="external" data-ajax="false" href="../../policy.html"><h3>プライバシーポリシー</h3></a></li>
</ul>
	<!--</div>--><!-- /content -->
<div style="clear: both;"></div>
<div data-role="footer" data-theme="c"><p class="center">&copy;Copyright Gokakuoh Inc. All rights reserved.</p></div>
</div><!-- /page -->
</body>
</html>
